function delta_all = robustTight_2(i_max,F_tild,Acl,D,w_dist_min,w_dist_max)
% Computes the robust constraint tightening delta_i (corresponds to robust
% stochastic constraint thightening with p=1) for steps i=1 to i=i_max
%
% IMPROVED VERSION: calls robust tightening computation once per row
% instead of separately for each entry. 

% IF NO MOSEK:
linprog_options = optimoptions('linprog','Display','none');


    if i_max < 1
        disp('i is too small!');
        delta_all = nan;
        return;
    end
    no_rows = size(F_tild,1);
    nw = size(D,2);                         % Dimension of the disturbance vector.
    
    % Obtain distribution support matrices Hw, hw of W
    Hw = zeros(2*nw,nw);
    hw = [];
    for l = 1:nw
        Hw(2*l-1,l) = 1;
        Hw(2*l,l) = -1;
        hw = [hw; w_dist_max; -w_dist_min];
    end
    
    delta_all = [];
    for j = 1:no_rows
        a_i_j_sum = 0;
        delta_all_j = [];
        for k = 1:i_max
            % compute a_temp_j using linprog
            f_atemp = (-F_tild(j,:)*Acl^(k-1)*D).';        % Need to max. real objective -> minus sign
            [~,fval,exitflag,output] = linprog(f_atemp,Hw,hw,[],[],[],[],linprog_options);
            a_temp = -fval;
            if exitflag == 1
                % Do nothing
            else
                disp(['Problem with optimization: exitflag = ',num2str(exitflag)]);
                disp(output.message);
            end
            a_i_j_sum = a_i_j_sum + a_temp;
            delta_all_j = [delta_all_j, a_i_j_sum];
        end
        delta_all = [delta_all; delta_all_j];
    end

end
