function [A_min, b_min] = computeMinConstraintSet(A,b)
% removes redundant half-space constraints from the set Q = {x | A*x <= b}
% by checking row by row whether the halfspace created by constraint row j 
% contains the set created from the remaining constraints (without row j 
% and all rows that were already deemed redundant).
%
% SYNTAX:
%       [A_min, b_min] = computeMinConstraintSet(A,b)
%
% INPUTS:
%       A, b:   Polyhedral constraint matrices
%
% OUTPUTS:
%       A_min, b_min:   Polyhedral constraint matrices without redundant
%       constraints: Q = {x | A_min*x <= b_min}

A_min_temp = [];
b_min_temp = [];
nc = size(A,1);     % number of halfspace constraints

for j = 1:nc
    % Constraint to be investigated
    A_check = A(j,:);
    b_check = b(j);
    
    % Remaining constraints (with constraints already deemed redundant
    % already removed)
    A_rest = A(j+1:end,:);
    A_rest = [A_min_temp; A_rest];
    b_rest = b(j+1:end);
    b_rest = [b_min_temp; b_rest];
    
    % Check
    [res, ~, ~] = subsetTest(A_rest,b_rest,A_check,b_check);
    if res == 1
        % row j is redundant -> remove
    elseif res == 0
        % row j is not redundant -> add constraint to array of already
        % checked constraints
        A_min_temp = [A_min_temp; A_check];
        b_min_temp = [b_min_temp; b_check];
    end
end

% RETURN
A_min = A_min_temp;
b_min = b_min_temp;