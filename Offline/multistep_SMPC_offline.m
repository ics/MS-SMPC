function ms_smpc_params = multistep_SMPC_offline(sys_params,Mmulti,vis)
% Function to obtain offline parameters and parameter struct for simple
% multistep SMPC; returns parameter struct to be fed into the classes
% multistep_SMPC or multistep_SMPC_economic.

switch nargin
    case 3
        
    case 2
        vis = 0;
    otherwise
        error('multistep_SMPC_offline: wrong number of arguments!');
end

if vis == 2
    vis_extreme = true;
else
    vis_extreme = false;
end

disp(' ');
disp('OFFLINE PARAMETERS: MULTISTEP SMPC');

%% Lookup for parameters to simplify expressions
N     = sys_params.mpc.N;
p     = sys_params.constraints.p;
Ns    = sys_params.stoch.Ns;
conf_param = sys_params.stoch.conf;
F_tilde    = sys_params.constraints.F_tilde;
Acl   = sys_params.sys.A + sys_params.sys.B * sys_params.sys.K;
D     = sys_params.sys.D;
pd    = sys_params.w.dist;
w_min = sys_params.w.dist_lower_bound;
w_max = sys_params.w.dist_upper_bound;


%% Compute Constraint Tightening beta_tilde for Multistep SMPC
disp('Compute constraint tightening...');

beta_tilde_all = multistepTight(p,N,Mmulti,Ns,conf_param,Acl,F_tilde,D,pd,w_min,w_max);


%% Compute Terminal Set
disp('Compute terminal set...');
if max(max(beta_tilde_all)) > 1
    error("multistep_SMPC_offline: max(beta_tilde_all) > 1 --> Tightening resulting in empty set");
end

[A_Zf_MS, b_Zf_MS, nu_MS] = computeTerminalSet_MS_SMPC(Acl,D,N,F_tilde,beta_tilde_all(:,N),w_min,w_max,vis);
% it is important, that beta_tilde_all(:,N) is handed over to the function,
% and not just beta_tilde_all(:,end), as the sequence might have more than
% N entries, but the tightening term corresponding to i = N-1 (Matlab index
% N) is relevant here.

%% Create Parameter Struct
disp('Create parameter struct...');

ms_smpc_params.sys = sys_params.sys;
ms_smpc_params.cost = sys_params.cost;
ms_smpc_params.constraints = sys_params.constraints;
ms_smpc_params.mpc = sys_params.mpc;
ms_smpc_params.w = sys_params.w;

ms_smpc_params.sys.Acl = Acl;

ms_smpc_params.constraints.beta_all = beta_tilde_all;
ms_smpc_params.constraints.A_Zf = A_Zf_MS;
ms_smpc_params.constraints.b_Zf = b_Zf_MS;

ms_smpc_params.mpc.Mmulti = Mmulti;

disp('DONE');
