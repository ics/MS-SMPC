function gamma_all = exactTight_2(p,Nbar,Ns,confParam,Acl,F_tild,D,pd,w_dist_min,w_dist_max,vis)
% Computes the stochastic constraint tightening gamma_k for all k=[0:Nbar]
%
% IMPROVED VERSION: calls robust tightening computation once per row
% instead of separately for each entry. 

    %% INITIAL CHECKS
    switch nargin
    case 11
        
    case 10
        vis = false;
    otherwise
        error('exactTight_2: wrong number of argunments');
    end
    
    if Nbar < 0
        error('exactTight_2: Nbar must be a non-negative integer!');
    end
    
    no_rows = size(F_tild,1);
    nw = size(D,2);                         % Dimension of the disturbance vector. 
    nx = size(D,1);                         % Dimension of the state vector
    gamma_all_int = ones(no_rows,Nbar)*nan;
%    gamma_all_robust = ones(no_rows,Nbar)*nan;
    r_vector = zeros(no_rows,1);
    robust_rows = zeros(no_rows,1);         % If p=1 for a row j, no_rows(j) = 1, else 0
    
    % Check if dimensions of p are correct
    if min(size(p)) ~= 1
        error('Probability vector p may not be a matrix!');
    end
    if length(p) == 1
        p_vec = ones(no_rows,1)*p;    % same probability for each row
    elseif length(p) == no_rows
        p_vec = p;
    else
        error('Probability vector p must have same number of entries as there are constraint rows!');
    end
    

    
    %% ROBUST TIGHTENING FOR ROWS WITH p=1
    for j = 1:no_rows
        if (p_vec(j) < 1) && (p_vec(j) > 0)
            % Do exact stochstic tightening
            % Compute r-value (allowed number of constraint violations)
            epsilon = 1-p_vec(j);
            r = epsilon*Ns - sqrt(2*epsilon*Ns*log(1/confParam));
            r = floor(r);       % Round down to obtain integer value
            if r < 0
                disp(['Row ', num2str(j),': Value of r is negative. Thus, r was set to 0']);
                r = 0;
            end
            r_vector(j) = r;
            
        elseif p_vec(j) == 1
            % Robust Constraint Tightening
            robust_rows(j) = 1;
            r_vector(j) = -1;
            gamma_all_int(j,:) = robustTight_2(Nbar,F_tild(j,:),Acl,D,w_dist_min,w_dist_max);
%             for k = 1:Nbar
%                 gamma_all_int(j,k) = robustTight(k,F_tild(j,:),Acl,D,w_dist_min,w_dist_max);
%             end
        else
            % ERROR: All probability entries must be in (0,1]
            error('exactTight_2: All probability values must lie in the interval (0,1]');
        end
    end
        
        
    %% EXACT STOCHASTIC CONSTRAINT TIGHTENING    
    
    % draw Ns disturbance sequences of length Nbar
    W_all = random(pd,nw*Ns,Nbar);
    
    % do rowwise
    for j = 1:no_rows
        if robust_rows(j) == 1;
            % skip
        else
            % Compute all RHS realizations for row j (over all realizations
            % and time steps)
            RHS_j = zeros(Ns,Nbar)*nan;
            for l = 1:Ns
                e_k = zeros(nx,1);
                for k = 1:Nbar
                    e_k = Acl*e_k + D*W_all(nw*l-(nw-1):nw*l,k);    % recursively update state error
                    RHS_j(l,k) = F_tild(j,:)*e_k;                   % determine corresponding gamma-RHS
                end
            end
            
            % Iterate through time steps k to obtain gamma_j(k)
            for k = 1:Nbar
                RHS_j_all_k = RHS_j(:,k);
                RHS_sorted = sort(RHS_j_all_k);
                gamma_all_int(j,k) = RHS_sorted(Ns-r_vector(j));
            end
        end
    end

gamma_all = [zeros(no_rows,1), gamma_all_int];     % add gamma_0 in the beginning.

end