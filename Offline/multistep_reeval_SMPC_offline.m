function rems_smpc_params = multistep_reeval_SMPC_offline(sys_params,Mmulti,vis)
% Function to obtain offline parameters and parameter struct for
% (regular/re-evaluating) Multistep SMPC (MS-SMPC or REMS-SMPC); returns
% parameter struct to be fed into the re-evaluating Multistep SMPC class
% Inputs: system parameter struct sys_params and multistep horizon length
% Mmulti.

switch nargin
    case 3
        
    case 2
        vis = 0;
    otherwise
        error('multistep_reeval_SMPC_offline: wrong number of arguments!');
end

if vis == 2
    vis_extreme = true;
else
    vis_extreme = false;
end

disp(' ');
disp('OFFLINE PARAMETERS: RE-EVALUATING MULTISTEP SMPC');

%% Lookup for parameters to simplify expressions
N     = sys_params.mpc.N;
p     = sys_params.constraints.p;
Ns    = sys_params.stoch.Ns;
conf_param = sys_params.stoch.conf;
F_tilde    = sys_params.constraints.F_tilde;
Acl   = sys_params.sys.A + sys_params.sys.B * sys_params.sys.K;
D     = sys_params.sys.D;
pd    = sys_params.w.dist;
w_min = sys_params.w.dist_lower_bound;
w_max = sys_params.w.dist_upper_bound;


%% Compute Constraint Tightening beta_tilde for Multistep SMPC
disp('Compute constraint tightening...');

beta_tilde_all = multistepTight(p,N,Mmulti,Ns,conf_param,Acl,F_tilde,D,pd,w_min,w_max);


%% Compute Terminal Set
disp('Compute terminal set...');
if max(max(beta_tilde_all)) > 1
    error("multistep_reeval_SMPC_offline: max(beta_tilde_all) > 1 --> Tightening resulting in empty set");
end

% it is important, that beta_tilde_all(:,N+predstep) is handed over to the function,
% and not just beta_tilde_all(:,end), as the sequence might have more than
% N+predstep entries, but the tightening term corresponding to i = N+predstep-1 (Matlab index
% N) is relevant here.

% Time-varying terminal set:
A_Zf_REMS = {};
b_Zf_REMS = {};
nu_REMS = {};
for pred_step = 0:Mmulti-1
    [A_Zf_REMS_temp, b_Zf_REMS_temp, nu_REMS_temp] = computeTerminalSet_MS_SMPC(Acl,D,N+pred_step,F_tilde,beta_tilde_all(:,N+pred_step),w_min,w_max,vis);
    A_Zf_REMS{pred_step+1} = A_Zf_REMS_temp;
    b_Zf_REMS{pred_step+1} = b_Zf_REMS_temp;
    nu_REMS{pred_step+1} = nu_REMS_temp;
end


%% Create Parameter Struct
disp('Create parameter struct...');

rems_smpc_params.sys = sys_params.sys;
rems_smpc_params.cost = sys_params.cost;
rems_smpc_params.constraints = sys_params.constraints;
rems_smpc_params.mpc = sys_params.mpc;
rems_smpc_params.w = sys_params.w;

rems_smpc_params.sys.Acl = Acl;

rems_smpc_params.constraints.beta_all = beta_tilde_all;
rems_smpc_params.constraints.A_Zf = A_Zf_REMS;
rems_smpc_params.constraints.b_Zf = b_Zf_REMS;

rems_smpc_params.mpc.Mmulti = Mmulti;

disp('DONE');
