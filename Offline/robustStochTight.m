function beta_i = robustStochTight(i,gamma_1,F_tild,Acl,D,w_dist_min,w_dist_max)
% Computes the robust stochastic constraint tightening at time i, i.e.
% beta_i.

% IF NO MOSEK:
linprog_options = optimoptions('linprog','Display','none');


    
    if i < 1
        disp('i is too small!');
        beta_i = nan;
        return;
    end
    no_rows = size(F_tild,1);
    nw = size(D,2);                         % Dimension of the disturbance vector.
    
    % Obtain distribution support matrices Hw, hw of W
    Hw = zeros(2*nw,nw);
    hw = [];
    for l = 1:nw
        Hw(2*l-1,l) = 1;
        Hw(2*l,l) = -1;
        hw = [hw; w_dist_max; -w_dist_min];
    end
    
    % For each a_m and each row j of a_m
    a_sum = 0;          % keeps track of the current sum of a's
%     a_debug = zeros(no_rows,i-1);     % DEBUG
    for m = 1:(i-1)
        a_m = zeros(no_rows,1);             % Initializes a_m
        for j = 1:no_rows
            % Solve the Optimization problem
            f_mj = (-F_tild(j,:)*Acl^m*D).';        % Need to max. real objective -> minus sign
            [w,fval,exitflag,output] = linprog(f_mj,Hw,hw,[],[],[],[],linprog_options);
            a_m(j) = -fval;                         % Want max. value -> need to take negative fval
            % DEBUG
            if exitflag == 1
                % Do nothing
            else
                disp(['Problem with optimization: exitflag = ',num2str(exitflag)]);
                disp(output.message);
            end
        end
        a_sum = a_sum + a_m;
%         a_debug(:,m) = a_m;   % DEBUG
    end
    
    % Return beta_i
    beta_i = gamma_1 + a_sum;
%     a_debug;          % DEBUG
end
