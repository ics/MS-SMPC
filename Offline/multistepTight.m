function beta_multi_i = multistepTight(p,N,Mmulti,Ns,conf_param,Acl,F_tild,D,pd,w_dist_min,w_dist_max)
% computes constraint tightening for multistep SMPC: beta_tilde_i for all
% i=[0:max([2*Mmulti-1,N-1+Mmulti-1])]
%
% SYNTAX:
% beta_multi_i = multistepTight(p,N,Mmulti,Ns,conf_param,Acl,F_tild,D,pd,w_dist_min,w_dist_max)
%


% IF NO MOSEK:
linprog_options = optimoptions('linprog','Display','none');
    
    if N < 1
        disp('N is too small!');
        beta_multi_i = nan;
        return;
    end
    no_rows = size(F_tild,1);
    nw = size(D,2);                 % Dimension of the disturbance vector
    
    % largest index for which tightening is to be computed
    i_max = max([2*Mmulti-1,N-1+Mmulti-1]);
    
    % Obtain distribution support matrices Hw, hw of W
    Hw = zeros(2*nw,nw);
    hw = [];
    for l = 1:nw
        Hw(2*l-1,l) = 1;
        Hw(2*l,l) = -1;
        hw = [hw; w_dist_max; -w_dist_min];
    end
    
    
   % exact stochastic tightening terms for i=0:Mmulti
   gamma_multi = exactTight_2(p,Mmulti,Ns,conf_param,Acl,F_tild,D,pd,w_dist_min,w_dist_max);
   % Postprocessing - to ensure monotonicity of tightening terms gamma_multi
   for i = 2:size(gamma_multi,2)
       for j = 1:size(gamma_multi,1)
           gamma_multi(j,i) = max(gamma_multi(j,i),gamma_multi(j,i-1));
       end
   end
   % additional robust terms a_i for i=Mmulti+1:i_max
   beta_multi_i = gamma_multi;      % containts beta_tilde_0 to beta_tilde_M
   beta_temp = gamma_multi(:,end);
   for i = Mmulti+1:i_max %N-1
       % Compute a_(i-1)
       a_temp = zeros(no_rows,1);
       for j=1:no_rows
           f_temp_j = (-F_tild(j,:)*Acl^(i-1)*D).';        % Need to max. real objective -> minus sign
           [~,fval,exitflag,output] = linprog(f_temp_j,Hw,hw,[],[],[],[],linprog_options);
           a_temp(j) = -fval;                         % Want max. value -> need to take negative fval
           % DEBUG
            if exitflag == 1
                % Do nothing
            else
                disp(['Problem with optimization: exitflag = ',num2str(exitflag)]);
                disp(output.message);
            end
       end
       beta_temp = beta_temp + a_temp;
       beta_multi_i = [beta_multi_i, beta_temp];
   end
   
end