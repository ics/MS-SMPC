function rs_mpc_params = robustStochastic_MPC_offline(sys_params,vis)
% Compute Tightening Terms and Terminal Set for robust stochastic MPC
% offline and return parameter struct to be fed into the RS_MPC class
% Input: system parameter struct sys_params. 

switch nargin
    case 2
        
    case 1
        vis = 0;
    otherwise
        error('robustStochastic_MPC_offline: wrong number of arguments!');
end

if vis == 2
    vis_extreme = true;
else
    vis_extreme = false;
end

disp(' ');
disp('OFFLINE PARAMETERS: ROBUST STOCHASTIC MPC');

%% Lookup for parameters to simplify expressions
N     = sys_params.mpc.N;
p     = sys_params.constraints.p;
Ns    = sys_params.stoch.Ns;
conf_param = sys_params.stoch.conf;
F_tilde    = sys_params.constraints.F_tilde;
Acl   = sys_params.sys.A + sys_params.sys.B*sys_params.sys.K;
D     = sys_params.sys.D;
pd    = sys_params.w.dist;
w_min = sys_params.w.dist_lower_bound;
w_max = sys_params.w.dist_upper_bound;


%% Compute Robust Stochastic Constraint Tightening
disp('Compute constraint tightening...');

gamma_1 = exactTight(1,p,Ns,conf_param,F_tilde,Acl,D,pd,w_min,w_max,vis_extreme);
beta_i = zeros(length(gamma_1), N-1);
for j = 1:N-1
    beta_i(:,j) = robustStochTight(j,gamma_1,F_tilde,Acl,D,w_min,w_max);
end
if max(max(beta_i)) > 1
    disp(['beta_i exceeds 1: ', num2str(max(max(beta_i)))]);
    error("max(max(beta_i)) > 1");
end


%% Compute Terminal Set
disp('Compute terminal set...');
[A_Zf_RS, b_Zf_RS, nu_RS] = computeTerminalSet(Acl,D,N,F_tilde,gamma_1,w_min,w_max,vis);


%% Create Parameter Struct
disp('Create parameter struct...');

rs_mpc_params.sys = sys_params.sys;
rs_mpc_params.cost = sys_params.cost;
rs_mpc_params.constraints = sys_params.constraints;
rs_mpc_params.mpc = sys_params.mpc;
rs_mpc_params.w = sys_params.w;

rs_mpc_params.sys.Acl = Acl;

rs_mpc_params.constraints.beta_i = beta_i;
rs_mpc_params.constraints.A_Zf = A_Zf_RS;
rs_mpc_params.constraints.b_Zf = b_Zf_RS;


disp('DONE!');
