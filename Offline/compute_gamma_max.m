function gamma_max = compute_gamma_max(gamma_last, Nbar, Acl, F_tilde, w_bar)
% computes the over-approximation of the exact stochastic tightening terms
% gamma: gamma_max >= gamma_k for all k. Nbar is the index of the last
% computed gamma. I.e., in application, we replace gamma_k by gamma_k =
% gamma_max for all k > Nbar.
if nargin ~= 5
    error('wrong number if input arguments');
end


% rho,c:
[T,Diag_f] = eig(Acl);
Tinv = inv(T);
c = norm(T,2)*norm(Tinv,2);
rho = max(max(abs(Diag_f)));

% rowwise norm of F_tild
nc = size(F_tilde,1);
F_tild_norm_rowwise = zeros(nc,1);
for j = 1:nc
    F_tild_norm_rowwise(j) = norm(F_tilde(j,:),2);
end

tail_factor = w_bar*c*rho^Nbar / (1 - rho);
tail = F_tild_norm_rowwise * tail_factor;
gamma_max = gamma_last + tail;

