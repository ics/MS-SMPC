function gamma_i = exactTight(i,p,Ns,confParam,F_tild,Acl,D,pd,w_dist_min,w_dist_max,vis)
% Computes exact stochastic constraint tightening at step i: gamma_i
% Use exactTight_2 to compute the entire tightening sequence gamma_k with
% k=[0:Nbar]

    if i < 1
        disp('i is too small!');
        gamma_i = nan;
        return;
    end
    no_rows = size(F_tild,1);
    gamma_i = zeros(no_rows,1);
    nw = size(D,2);                         % Dimension of the disturbance vector. 
    r_vector = [];
    
    % Check if dimensions of p are correct
    if min(size(p)) ~= 1
        error('Probability vector p may not be a matrix!');
    end
    if length(p) == 1
        p_vec = ones(no_rows,1)*p;    % same probability for each row
    elseif length(p) == no_rows
        p_vec = p;
    else
        error('Probability vector p must have same number of entries as there are constraint rows!');
    end
    
%     % Obtain distribution support matrices Hw, hw of W for robust
%     % tightening
%     Hw = zeros(2*nw,nw);
%     hw = [];
%     for l = 1:nw
%         Hw(2*l-1,l) = 1;
%         Hw(2*l,l) = -1;
%         hw = [hw; w_dist_max; -w_dist_min];
%     end
        
    for j = 1:no_rows                       % do every row of F_tild individually
        gamma_j_cand = zeros(1,Ns);         % array for candidate solutions gamma_j
        
        if (p_vec(j) < 1) && (p_vec(j) > 0)
        % STOCHASTIC TIGHTENING
            % Obtain r as the maximum possible value for r, given epsilon_upper = 1-p
            epsilon = 1-p_vec(j);
            r = epsilon*Ns - sqrt(2*epsilon*Ns*log(1/confParam));
            r = floor(r);       % Round down to obtain integer value
            if r < 0
                disp(['Row ', num2str(j),': Value of r is negative. Thus, r was set to 0']);
                r = 0;
            end
            r_vector = [r_vector; r];

            for l = 1:Ns                        % do every disturbance realization individually
                 W = random(pd,nw,i);           % Draw disturbance sequency of length i
                 % "build up" F_tild(j)*e(i)
                 gamma_j_LHS = 0;
    %              gamma_test = 0; %DEBUG
                 for m = 1:i
                     gamma_j_LHS = gamma_j_LHS + F_tild(j,:)*Acl^(i-m)*D*W(:,m);
    %                  gamma_test = gamma_test + F_tild(j,:)*Acl^(m-1)*D*W(:,m); %DEBUG
                 end
                 gamma_j_cand(l) = gamma_j_LHS;
    %              gamma_cand_test(l) = gamma_test; %DEBUG
            end
    %         sum(abs(gamma_j_cand - gamma_cand_test)) %DEBUG

            % Find optimal gamma_j
            gamma_j_sort = sort(gamma_j_cand);  % Sort candidate solutions by ascending value
            optimal_index = Ns-r;               % Last allowed element
            gamma_j_opt = gamma_j_sort(optimal_index);

            % Add optimal solution to output vector
            gamma_i(j) = gamma_j_opt;           
            
        elseif p_vec(j) == 1
        % ROBUST TIGHTENING
            disp(['Row ', num2str(j),': call robustTight()']);
            % call robustTight()
            gamma_i(j) = robustTight(i,F_tild(j,:),Acl,D,w_dist_min,w_dist_max);
            r_vector = [r_vector; -1];  % set r to -1 to indicate robust tightening
        else
        % ERROR: All probability entries must be in (0,1]
            error('All probability values must lie in the interval (0,1]');
        end


    end         % END iteration over rows
%     r_vector
end