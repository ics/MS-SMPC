function [A_terminal, b_terminal, nu] = computeTerminalSet_MS_SMPC(Acl, D, N, F_tild, beta_tilde_last, w_dist_min, w_dist_max,vis)
% Function to compute the terminal set for multistep SMPC (MS-SMPC) with terminal
% control law u = Kx
%
% can also be used to compute the time-varying terminal sets for REMS SMPC
%
% hand over: beta_tilde_last = \tilde{\beta}_{N-1}


% IF NO MOSEK:
linprog_options = optimoptions('linprog','Display','none');

switch nargin
    case 8
        % do nothing
    case 7
        vis = false;
    otherwise
        error('computeTerminalSet_MS_SMPC: wrong number of argunments');
end


disp_debug = vis;
nc = size(F_tild,1);        % number of half-space constraints
nw = size(D,2);                 % Dimension of the disturbance vector
    
% Obtain distribution support matrices Hw, hw of W
Hw = zeros(2*nw,nw);
hw = [];
for l = 1:nw
    Hw(2*l-1,l) = 1;
    Hw(2*l,l) = -1;
    hw = [hw; w_dist_max; -w_dist_min];
end

% First a_i for beta_tilde_i=N
a_temp = zeros(nc,1);
for j = 1:nc
    f_temp_j = (-F_tild(j,:)*Acl^(N-1)*D).';
    [~,fval,exitflag,output] = linprog(f_temp_j,Hw,hw,[],[],[],[],linprog_options);
    a_temp(j) = -fval;
    % DEBUG
    if exitflag == 1
        % Do nothing
    else
        disp(['Problem with optimization: exitflag = ',num2str(exitflag)]);
        disp(output.message);
    end
end

% Initialize
beta_temp = beta_tilde_last + a_temp;
if max(beta_temp) > 1
    error("computeTerminalSet_MS_SMPC: max(beta_temp)) > 1");
end
A_temp = F_tild;
b_temp = ones(nc,1) - beta_temp;
i = 0;
converged = 0;

while converged ~= 1
    % DEBUG
    if disp_debug == true
%         pause(1)
        disp(['Iteration ',num2str(i)]);
    end
    
    i = i+1;
    a_next = zeros(nc,1);
    for j = 1:nc
        f_temp_j = (-F_tild(j,:)*Acl^(N+i-1)*D).';
        [~,fval,exitflag,output] = linprog(f_temp_j,Hw,hw,[],[],[],[],linprog_options);
        a_next(j) = -fval;
        % DEBUG
        if exitflag == 1
            % Do nothing
        else
            disp(['Problem with optimization: exitflag = ',num2str(exitflag)]);
            disp(output.message);
        end
    end
    beta_temp = beta_temp + a_next;
    if max(beta_temp) > 1
        disp(['Step i = ',num2str(i)]);
        error("computeTerminalSet_MS_SMPC: max(beta_temp)) > 1");
    end
    A_next = F_tild * Acl^i;
    b_next = ones(nc,1) - beta_temp;
    
    % check row-by-row if current set A_temp,b_temp is contained in
    % A_next, b_next 
    no_next_constr = nc;
    is_red = -1*ones(no_next_constr,1);
    
    for j = 1:no_next_constr
        %disp(['Constraint ',num2str(j)]);       % DEBUG
        [res,~,~] = subsetTest(A_temp,b_temp,A_next(j,:),b_next(j));
        is_red(j) = res;
    end
if disp_debug == true
    is_red
end
    % Evaluate results
    violations = length(find(is_red~=1));
    if violations == 0
        % X_temp is subset of X_next -> terminal set is complete 
        converged = 1;
    elseif sum(isnan(is_red)) ~= 0
        % ERROR - something went wrong
        error('computeTerminalSet: Something went wrong! Some subset test(s) returned NaN');
    else
        % add all rows that have is_red(j) = 0 to constraints
        %for j = 1:no_next_constr
            %if is_red(j) == 0
                A_temp = [A_temp; A_next];
                b_temp = [b_temp; b_next];
                %A_temp = [A_temp; A_next(j,:)];
                %b_temp = [b_temp; b_next(j)];
            %end
        %end
    end
end

% remove redundant constraints from final set
%[A_final,b_final] = computeMinConstraintSet(A_temp,b_temp);

% RETURN
A_terminal = A_temp;%A_final;
b_terminal = b_temp;%b_final;
nu = i;

if disp_debug == true && size(Acl,2) <= 3
    disp(['Converged at iteration ',num2str(i)]);
end