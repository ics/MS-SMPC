function [A_terminal, b_terminal, nu] = computeTerminalSet(Acl, D, N, F_tild, gamma_1, w_dist_min, w_dist_max,vis)
% Function to compute the terminal set for robust stochastic MPC (RS-MPC)
% with dynamics under terminal controller u = K*x
%    x_next = Acl*x + D*w    where Acl = A+B*K, and Acl as. stable
% w is the disturbance which is in a set bounded by w_dist_min and
% w_dist_max.
% 
% SYNTAX:
%        [A_terminal, b_terminal, nu] = computeTerminalSet(Acl, D, N, F_tild, gamma_1, w_dist_min, w_dist_max,vis) 
%
% INPUTS:
%         Acl: Closed-loop system dynamics Acl = A+B*K
%         D:   Disturbance input matrix
%         F_tild: Augmented constraint matrix for chance constraints
%         N:   Horizon length
%         gamma_1:  exact stochastic tightening term for i=1
%         w_dist_min: lower bound of disturbance support
%         w_dist_max: upper bound of disturbance support
%         vis: display and plot debug information true/{false}
%
% OUTPUTS: 
%         A_terminal, b_terminal: parameters for terminal set
%         {s | A_terminal*s <= b_terminal}
%         nu:  index of largest constraint set

switch nargin
    case 8
        % do nothing
    case 7
        vis = false;
    otherwise
        error('computeTerminalSet: wrong number of argunments');
end



disp_debug = vis;

nc = size(F_tild,1);        % number of half-space constraints


% INITIALIZE
beta_0 = robustStochTight(N,gamma_1,F_tild,Acl,D,w_dist_min,w_dist_max);
A_temp = F_tild;
b_temp = ones(nc,1) - beta_0;
i = 1;
converged = 0;

% LOOP
while converged~=1
    % DEBUG
    if disp_debug == true
%         pause(1)
        disp(['Iteration ',num2str(i)]);
    end
    
    
    beta_next = robustStochTight(N+i,gamma_1,F_tild,Acl,D,w_dist_min,w_dist_max);
    if max(beta_next) > 1
        error("computeTerminalSet: max(beta_next)) > 1");
    end
    A_next = F_tild * Acl^i;
    b_next = ones(nc,1) - beta_next;
    
    % check row-by-row if current set A_temp,b_temp is contained in
    % A_next, b_next 
    no_next_constr = nc;
    is_red = -1*ones(no_next_constr,1);
    
    for j = 1:no_next_constr
        if disp_debug == true
            disp(['Constraint ',num2str(j)]);       % DEBUG
        end
        [res,~,~] = subsetTest(A_temp,b_temp,A_next(j,:),b_next(j));
        is_red(j) = res;
    end
if disp_debug == true
    is_red
end
    % Evaluate results
    violations = length(find(is_red~=1));
    if violations == 0
        % X_temp is subset of X_next -> terminal set is complete 
        converged = 1;
    elseif sum(isnan(is_red)) ~= 0
        % ERROR - something went wrong
        error('computeTerminalSet: Something went wrong! Some subset test(s) returned NaN');
    else
        % add all rows that have is_red(j) = 0 to constraints
        for j = 1:no_next_constr
            if is_red(j) == 0
                A_temp = [A_temp; A_next(j,:)];
                b_temp = [b_temp; b_next(j)];
            end
        end
        i = i+1;
        
    end   
end

% remove redundant constraints from final set
[A_final,b_final] = computeMinConstraintSet(A_temp,b_temp);

% RETURN
A_terminal = A_final;
b_terminal = b_final;
nu = i-1;

if disp_debug == true
    disp(['Converged at iteration ',num2str(i)]);
end
