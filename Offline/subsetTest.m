function [res, supports, supports_bool] = subsetTest(A_sub, b_sub, A_outer, b_outer,tolerance)
% Checks whether Set P = {x | A_sub*x <= b_sub} is a subset of
% Q = {x | A_outer*x <= b_outer} using the support
%
% SYNTAX:
%         [res, supports, supports_bool] = subsetTest(A_sub, b_sub, A_outer, b_outer)
% 
% OUTPUT: res = true (1)   -  P is subset of Q
%         res = false (0)  -  P is not a subset of Q
%         supports: contains the support value of each row of A_outer
%         supports_bool: true if P is contained in corresponding halfspace
%                        false otherwise

% IF NO MOSEK:
linprog_options = optimoptions('linprog','Display','none');


    switch nargin
    case 5
        
    case 4
        tolerance = 1e-9;   % EXPERIMENTAL
    otherwise
        error('subsetTest: wrong number of argunments');
end

nc_outer = size(A_outer,1);     % no of rows in A_outer, i.e. no of halfspaces
h = ones(nc_outer,1)*nan;          % array for support values
h_res = ones(nc_outer,1)*nan;      % boolean array for results
for i = 1:nc_outer
    f_lp = -A_outer(i,:).';
    A_lp = A_sub;
    b_lp = b_sub;
    
    [x_temp, h_temp, exitflag,output]  = linprog(f_lp, A_lp, b_lp,[],[],[],[],linprog_options);
    h_temp = -h_temp;   % h = max Di*x = - min -Di*x
    if exitflag ~= 1 && exitflag ~= -3
        warning("linprog did not converge!");
        disp(['exitflag: ',num2str(exitflag)]);
        disp('continue');
    elseif exitflag == -3       % corresponds to an unbounded problem
        %disp('Problem is unbounded. Thus, h_P > d and set X_sup is not a subset of X_outer');
        if isempty(find(f_lp~=0))
            error("ISSUE: subsetTest: objective vector f_lp = 0! Thus, the problem can be unbounded despite the constraints. - No procedure has been implemented for this case so far. Thus, code terminates with an error");
        end
        % P is not contained in halfspace Q_i
        h(i) = nan;
        h_res(i) = false;
    elseif h_temp <= b_outer(i) + tolerance
        % P is contained in halfspace Q_i
        h(i) = h_temp;
        h_res(i) = true;
    elseif h_temp > b_outer(i) + tolerance          %%
        % P is not contained in halfspace Q_i
        h(i) = h_temp;
        h_res(i) = false;
    end
end

no_violations = find(h_res~=1);
if length(no_violations) == 0
    % P is contained in Q / P is a subset of Q
    res = true;
else
    % P is not contained in Q / P is not a subset of Q
    res = false;
end

supports = h;
supports_bool = h_res;
