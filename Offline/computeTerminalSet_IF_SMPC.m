function [A_terminal, b_terminal, nu] = computeTerminalSet_IF_SMPC(Acl, F_tild, gamma_max, vis)
% Function to compute the terminal set for indirect feedback SMPC (IF-SMPC)  
% with dynamics under terminal controller u = K*x
%    x_next = Acl*x + D*w    where Acl = A+B*K, and Acl as. stable
% where w is the disturbance which is in a set bounded by w_dist_min and
% w_dist_max.
% 
% SYNTAX:
%        [A_terminal, b_terminal, nu] = computeTerminalSet_IF_SMPC(Acl, F_tild, gamma_max, vis) 
%
% INPUTS:
%         Acl: Closed-loop system dynamics Acl = A+B*K
%         F_tild: Augmented constraint matrix for chance constraints
%         gamma_max:  over-approximation of exact stochastic tightening
%                       term, s.t. gamma_max >= gamma_k for all k
%         vis: display and plot debug information true/{false}
%
% OUTPUTS: 
%         A_terminal, b_terminal: parameters for terminal set
%         {s | A_terminal*s <= b_terminal}
%         nu:  index of largest constraint set

switch nargin
    case 4
        % do nothing
    case 3
        vis = false;
    otherwise
        error('computeTerminalSet_IF_SMPC: wrong number of argunments');
end

disp_debug = vis;

nc = size(F_tild,1);        % number of half-space constraints


% INITIALIZE
%%%%%%%                     beta_0 = robustStochTight(N,gamma_1,F_tild,Acl,D,w_dist_min,w_dist_max);
A_temp = F_tild;
b_temp = ones(nc,1) - gamma_max;
i = 1;
converged = 0;

% LOOP
while converged~=1
    % DEBUG
    if disp_debug == true
%         pause(1)
        disp(['Iteration ',num2str(i)]);
    end
    
    
%%%%%%%                         beta_next = robustStochTight(N+i,gamma_1,F_tild,Acl,D,w_dist_min,w_dist_max);
    A_next = F_tild * Acl^i;
    b_next = ones(nc,1) - gamma_max;
    
    % check row-by-row if current set A_temp,b_temp is contained in
    % A_next, b_next 
    no_next_constr = nc;
    is_red = -1*ones(no_next_constr,1);
    
    for j = 1:no_next_constr
        [res,~,~] = subsetTest(A_temp,b_temp,A_next(j,:),b_next(j));
        is_red(j) = res;
    end
    
    % Evaluate results
    violations = length(find(is_red~=1));
    if violations == 0
        % X_temp is subset of X_next -> terminal set is complete 
        converged = 1;
    elseif sum(isnan(is_red)) ~= 0
        % ERROR - something went wrong
        error('computeTerminalSet: Something went wrong! Some subset test(s) returned NaN');
    else
        % add all rows that have is_red(j) = 0 to constraints
        %for j = 1:no_next_constr
        %    if is_red(j) == 0
        %        A_temp = [A_temp; A_next(j,:)];
        %        b_temp = [b_temp; b_next(j)];
        %    end
        %end
        A_temp = [A_temp; A_next];
        b_temp = [b_temp; b_next];
        i = i+1;
        
    end   
end

% remove redundant constraints from final set
%[A_temp,b_temp] = computeMinConstraintSet(A_temp,b_temp);

% RETURN
A_terminal = A_temp;
b_terminal = b_temp;
nu = i-1;

if disp_debug == true
    disp(['Converged at iteration ',num2str(i)]);
end
