function if_smpc_params = indirectFeedback_SMPC_offline(sys_params,Nbar,vis)
% Compute Tightening Terms and Terminal Set for IF-SMPC offline, and return
% parameter struct to be fed into the IF-SMPC class.
% Input: system parameter struct provided by system parameter function, and
% Nbar (index of largest computed gamma_k - \bar(k) in the paper). 

switch nargin
    case 3
        
    case 2
        vis = 0;
    otherwise
        error('indirectFeedback_SMPC_offline: wrong number of arguments!');
end

if vis == 2
    vis_extreme = true;
else
    vis_extreme = false;
end

disp(' ');
disp('OFFLINE PARAMETERS: INDIRECT FEEDBACK SMPC');

%% Lookup for parameters to simplify expressions
N     = sys_params.mpc.N;
p     = sys_params.constraints.p;
Ns    = sys_params.stoch.Ns;
conf_param = sys_params.stoch.conf;
F_tilde    = sys_params.constraints.F_tilde;
Acl   = sys_params.sys.A + sys_params.sys.B*sys_params.sys.K;
D     = sys_params.sys.D;
pd    = sys_params.w.dist;
w_min = sys_params.w.dist_lower_bound;
w_max = sys_params.w.dist_upper_bound;
w_bar = sys_params.w.w_max_norm;


%% Compute Exact Stochastic Constraint Tightening
disp('Compute constraint tightening...');

gamma_i = exactTight_2(p,Nbar,Ns,conf_param,Acl,F_tilde,D,pd,w_min,w_max);
gamma_i_unchanged = gamma_i;

% Postprocessing - to ensure monotonicity of tightening terms gamma_i
for i = 2:size(gamma_i,2)
    for j = 1:size(gamma_i,1)
        gamma_i(j,i) = max(gamma_i(j,i),gamma_i(j,i-1));
    end
end


%% Compute Upper bound gamma_max
disp('Compute upper bound gamma_max...');

% rho,c:
[T,Diag_f] = eig(Acl);
Tinv = inv(T);
c = norm(T,2)*norm(Tinv,2);
rho = max(max(abs(Diag_f)));

% rowwise norm of F_tild
nc = size(F_tilde,1);
F_tild_norm_rowwise = zeros(nc,1);
for j = 1:nc
    F_tild_norm_rowwise(j) = norm(F_tilde(j,:),2);
end

tail_factor = w_bar*c*rho^Nbar / (1 - rho);
tail = F_tild_norm_rowwise * tail_factor;
gamma_max = gamma_i(:,Nbar+1) + tail;


%% Compute Terminal Set
disp('Compute terminal set...');

% Check if all tightening terms are smaller/equal 1
if max(gamma_max) > 1
    disp(num2str(gamma_max));
    error("indirectFeedback_SMPC_offline: max(gamma_max) > 1 --> Tightening resulting in empty set");
end


[A_Zf_IF, b_Zf_IF, nu_IF] = computeTerminalSet_IF_SMPC(Acl,F_tilde,gamma_max,vis);



%% Plot gamma for comparison
if vis ~= 0
    figure();
    sgtitle('gamma_i');

    subplot(1,2,1);
    title('Component 1');
    hold on;
    plot(0:Nbar,gamma_i(1,:));
    plot(0:Nbar,gamma_i_unchanged(1,:),'--');
    yline(gamma_max(1),'r--','linewidth',1);
    legend('monotone','original','gamma_max');
    grid on
    axis padded

    subplot(1,2,2);
    title('Component 2');
    hold on;
    plot(0:Nbar,gamma_i(2,:));
    plot(0:Nbar,gamma_i_unchanged(2,:),'--');
    yline(gamma_max(2),'r--','linewidth',1);
    % legend('monotone','original','gamma_max');
    grid on
    axis padded
end

%% Create Parameter Struct
disp('Create parameter struct...');

if_smpc_params.sys = sys_params.sys;
if_smpc_params.cost = sys_params.cost;
if_smpc_params.constraints = sys_params.constraints;
if_smpc_params.mpc = sys_params.mpc;
if_smpc_params.w = sys_params.w;

if_smpc_params.sys.Acl = Acl;

if_smpc_params.constraints.gamma_all = gamma_i;
if_smpc_params.constraints.gamma_max = gamma_max;
if_smpc_params.constraints.Nbar = Nbar;
if_smpc_params.constraints.A_Zf = A_Zf_IF;
if_smpc_params.constraints.b_Zf = b_Zf_IF;


disp('DONE!');