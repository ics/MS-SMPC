classdef multistep_reeval_SMPC
% (regular / re-evaluating) multistep SMPC (MS-SMPC / REMS-SMPC)
% Multistep prediction over multistep horizon Mmulti, but problem is
% re-evaluated at every time step.
% Implementation for (regular) quadratic cost

    properties
        % Properties
        vis         % true/{false} - visualization + terminal outputs on/off
        N           % Prediction horizon
        Mmulti      % Multistep horizon
        
        nx          % State dimension
        nu          % Input dimension
        nw          % Disturbance dimension
        
        A           % Dynamics: state matrix
        B           % Dynamics: input matrix
        D           % Dynamics: disturbance matrix
        K           % Linear feedback controller
        Acl         % Closed-loop dynamics Acl = A+B*K
        
        Q           % State cost parameter
        R           % Input cost parameter
        P           % Terminal cost parameter
        
        % Probabilistic constraints 
        % of the form F_tilde*x + G*c <= 1 - beta_all
        F   
        F_tilde
        G
        Hx
        hx
        beta_tilde_all    % Constraint tightening terms for multistep prediction
        nc          % number of (combined) half-space constraints
        
        % Terminal set constraint
        % Z_f(k) = {s | A_Zf{k+1} * x <= b_Zf{k+1}}
        A_Zf    % Cell arrays containing terminal set parameters
        b_Zf
        
        % Potential hard input constraints
        % A_U0 * x_0 <= b_U0
        A_U0
        b_U0
        
        % Properties regarding the disturbance w
        w
        
        % Yalmip Object
        solvers         % 2*Mmulti solvers (M_k = 0 to M_k = 2*Mmulti-1)
    end
    
    
    methods
        function obj = multistep_reeval_SMPC(smpc_params, vis)
            
            switch nargin
                case 2
                    
                case 1
                    vis = false;
                otherwise
                    error('multistep_reeval_SMPC: wrong number of arguments');
            end
            
            %%%% INTERNAL PARAMETERS %%%%
            obj.N = smpc_params.mpc.N;
            obj.Mmulti = smpc_params.mpc.Mmulti;
            
            obj.nx = smpc_params.sys.nx;
            obj.nu = smpc_params.sys.nu;
            obj.nw = smpc_params.sys.nw;
            
            obj.A = smpc_params.sys.A;
            obj.B = smpc_params.sys.B;
            obj.D = smpc_params.sys.D;
            obj.K = smpc_params.sys.K;
            obj.Acl = smpc_params.sys.Acl;
            
            obj.Q = smpc_params.cost.Q;
            obj.R = smpc_params.cost.R;
            obj.P = smpc_params.cost.P;
            
            obj.F = smpc_params.constraints.F;
            obj.F_tilde = smpc_params.constraints.F_tilde;
            obj.G = smpc_params.constraints.G;
            obj.Hx = smpc_params.constraints.Hx;
            obj.hx = smpc_params.constraints.hx;
            obj.beta_tilde_all = smpc_params.constraints.beta_all;
            obj.nc = size(obj.F,1);
            
            obj.A_Zf = smpc_params.constraints.A_Zf;
            obj.b_Zf = smpc_params.constraints.b_Zf;
            
            obj.A_U0 = smpc_params.constraints.A_U0;
            obj.b_U0 = smpc_params.constraints.b_U0;
            
            obj.w = smpc_params.w;
            
            
            %%%% SANITY CHCEK %%%%
            if obj.Mmulti >= obj.N
                error('multistep_reeval_SMPC: multistep horizon must be smaller than MPC horizon!');
            end
            
            
            %%%% YALMIP OPTIONS %%%%
%%% possibly add 'solver','mosek' to list
            if vis == true
                verbose_setting = 2;
            else
                verbose_setting = 0;
            end
% DEBUG
% verbose_setting = 2       % CAN FORCE OUTPUTS
            yalmip_options = sdpsettings('verbose',verbose_setting);
            
            
            %%%% SOLVERS %%%%
            % Create 2*Mmulti solver objects
            solvers_temp = {};
            for M_k = 0:(2*obj.Mmulti-1)
                pred_step = mod(M_k, obj.Mmulti);
%                 A_Xf_temp = obj.A_Zf;
%                 b_Xf_temp = obj.b_Zf;
                A_Xf_temp = obj.A_Zf{pred_step + 1};
                b_Xf_temp = obj.b_Zf{pred_step + 1};
                
                % Optimization Variables
                xbar   = sdpvar(obj.nx, obj.N+1, 'full');
                s      = sdpvar(obj.nx, max([2*obj.Mmulti-M_k, obj.N+1]), 'full');
                x_cost = sdpvar(obj.nx, obj.N+1, 'full');
                ubar   = sdpvar(obj.nu, obj.N, 'full');
                c      = sdpvar(obj.nu, max([2*obj.Mmulti-M_k, obj.N]), 'full');
                u_cost = sdpvar(obj.nu, obj.N, 'full');
                
                % Optimization Variables (to be fixed as parameters)
                xbar_0 = sdpvar(obj.nx, 1, 'full');
                s_0    = sdpvar(obj.nx, 1, 'full');
                x_0    = sdpvar(obj.nx, 1, 'full');
                
                % Objective
                objective = 0;
                for i = 1:obj.N
                    objective = objective + x_cost(:,i).'*obj.Q*x_cost(:,i) + u_cost(:,i).'*obj.R*u_cost(:,i);
                end
                objective = objective + x_cost(:,obj.N+1).'*obj.P*x_cost(:,obj.N+1);
                
                % Constraints
                constraints = [];
                constraints = [constraints, xbar(:,1) == xbar_0];
                for i = 1:obj.N
                    constraints = [constraints, xbar(:,i+1) == obj.A*xbar(:,i) + obj.B*ubar(:,i)];
                    constraints = [constraints, ubar(:,i) == obj.K*xbar(:,i) + c(:,i)];
                end
                for i = (2*obj.Mmulti-M_k+1):obj.N
                    constraints = [constraints, obj.F_tilde*xbar(:,i) + obj.G*c(:,i) <= 1 - obj.beta_tilde_all(:,i+pred_step)];
                end
                constraints = [constraints, A_Xf_temp*xbar(:,obj.N+1) <= b_Xf_temp];
                
                constraints = [constraints, s(:,1) == s_0];
                for i = 1:(2*obj.Mmulti-M_k-1)
                    constraints = [constraints, s(:,i+1) == obj.Acl*s(:,i) + obj.B*c(:,i)];
                end
%implement constraints for i=1 seperately!
for j=1:obj.nc
    if norm(obj.G(j,:))>0
    %   constraints = [constraints, obj.F_tilde(j,:)*s(:,1) + obj.G(j,:)*c(:,1) <= 1 - obj.beta_tilde_all(j,1+M_k)] ;
     constraints = [constraints, obj.F_tilde(j,:)*x_0 + obj.G(j,:)*c(:,1) <= 1] ;
    end
end
%                 for i = 1:(2*obj.Mmulti-M_k)
                for i = 2:(2*obj.Mmulti-M_k)
                    constraints = [constraints, obj.F_tilde*s(:,i) + obj.G*c(:,i) <= 1 - obj.beta_tilde_all(:,i+M_k)];
                end
                
                constraints = [constraints, x_cost(:,1) == x_0];
                for i = 1:obj.N
                    constraints = [constraints, x_cost(:,i+1) == obj.A*x_cost(:,i) + obj.B*u_cost(:,i)];
                    constraints = [constraints, u_cost(:,i) == obj.K*x_cost(:,i) + c(:,i)];
                end
                
                % Settings
                inputs = { x_0, xbar_0, s_0};
                outputs = { [c], [s], [ubar], [xbar], [x_cost], [u_cost] };
                options = yalmip_options;
                
                % Create solver object
                solvers_temp{M_k+1} = optimizer(constraints, objective, options, inputs, outputs);
            end
            
            obj.solvers = solvers_temp;
        end
        
        
        function [c_opt, s_opt, ubar_opt, xbar_opt, x_cost_opt, u_cost_opt, solution_error] = solve(obj, x_k, xbar_k, s_k, k)
            if k < obj.Mmulti
                M_k = k;
            else
                M_k = obj.Mmulti + mod(k,obj.Mmulti);
            end
            current_solver = obj.solvers{M_k + 1};
            
            [results, solution_error] = current_solver(x_k,xbar_k,s_k);
            
            % Extract relevant data 
            c_opt = results{1};
            s_opt = results{2};
            ubar_opt = results{3};
            xbar_opt = results{4};
            x_cost_opt = results{5};
            u_cost_opt = results{6};

            % Check for issues
            if solution_error==0
%                 disp(yalmiperror(solution_error));
            else
                disp(['solver M_k = ',num2str(M_k)]);
                disp(yalmiperror(solution_error));
            end
        end
        
    end
end


                
            
            
    