classdef indirectFeedback_SMPC_SF_economic
% Indirect Feedback implementation with input parametrisation u = Kx + c
% Implementation for economic cost. 

    properties
        vis         % true/{false} - visualization + terminal outputs on/off
        N           % Prediction horizon
        
        nx          % State dimension
        nu          % Input dimension
        nw          % Disturbance dimension
        
        A           % Dynamics: state matrix
        B           % Dynamics: input matrix
        D           % Dynamics: disturbance matrix
        K           % Linear feedback controller
        Acl         % Closed-loop dynamics Acl = A+B*K
        
        Q           % State cost parameter
        R           % Input cost parameter
        P           % Terminal cost parameter
        
        % Probabilistic constraints 
        % Z^(i) = {[s;c] | [F_tilde G] * [s;c] < 1 - gamma_all(i+k)}
        F   
        F_tilde
        G
        Hx
        hx
        gamma_all
        gamma_max   % Upper bound on exact stochastic tightening terms
        Nbar        % Index of largest computed tightening term
        nc          % number of (combined) half-space constraints
        
        % Terminal set constraint
        % Z_f = {s | A_Zf * s <= b_Zf}
        A_Zf
        b_Zf
        
        % Properties regarding the disturbance w
        w
        
        % Yalmip Object
        solver
        
    end
    
    methods
        function obj = indirectFeedback_SMPC_SF_economic(smpc_params,vis)
            switch nargin
                case 3
                    
                case 2
                    vis = false;
                otherwise
                    error('indirectFeedback_SMPC_SF_economic: wrong number of argunments');
            end
            
            %%%% INTERNAL PARAMETERS %%%%
            obj.vis = vis;
            
            %%%% LOAD DATA INTO THE CALSS %%%%
            obj.N = smpc_params.mpc.N;
            
            obj.nx = smpc_params.sys.nx;
            obj.nu = smpc_params.sys.nu;
            obj.nw = smpc_params.sys.nw;
            
            obj.A = smpc_params.sys.A;
            obj.B = smpc_params.sys.B;
            obj.D = smpc_params.sys.D;
            obj.K = smpc_params.sys.K;
            obj.Acl = obj.A + obj.B*obj.K;
            
            obj.Q = smpc_params.cost.Q;
            obj.R = smpc_params.cost.R;
            obj.P = smpc_params.cost.P;
            
            obj.F = smpc_params.constraints.F;
            obj.F_tilde = smpc_params.constraints.F_tilde;
            obj.G = smpc_params.constraints.G;
            obj.Hx = smpc_params.constraints.Hx;
            obj.hx = smpc_params.constraints.hx;
            obj.gamma_all = smpc_params.constraints.gamma_all;
            obj.gamma_max = smpc_params.constraints.gamma_max;
            obj.Nbar = smpc_params.constraints.Nbar;
            obj.nc = size(obj.F,1);
            
            obj.A_Zf = smpc_params.constraints.A_Zf;
            obj.b_Zf = smpc_params.constraints.b_Zf;
            
            obj.w = smpc_params.w;
            
            
            
            %%%% CREATE YALMIP OBJECT %%%%
            % Optimization Variables
            xbar = sdpvar(obj.nx, obj.N+1, 'full');     % expected state
            s    = sdpvar(obj.nx, obj.N+1, 'full');     % nominal state
            ubar = sdpvar(obj.nu, obj.N, 'full');       % expected input
            c    = sdpvar(obj.nu, obj.N, 'full');       % nominal input
            % Optimization Variables (to be fixed as parameters)
            xbar_0 = sdpvar(obj.nx, 1, 'full');     % initial expected state
            s_0    = sdpvar(obj.nx, 1, 'full');     % initial nominal state
            gamma_i= sdpvar(obj.nc, obj.N, 'full');   % tighenting terms
            
            % Objective
%             objective = 0;
%             for i = 1:obj.N         % Stage Cost
%                 objective = objective + xbar(:,i).'*obj.Q*xbar(:,i) + ubar(:,i).'*obj.R*ubar(:,i);
%             end
%             objective = objective + xbar(:,obj.N+1).'*obj.P*xbar(:,obj.N+1);    % Terminal Cost

% EDGE CASE WITH ECONOMIC COST:
            objective = 0;
            for i = 1:obj.N
                objective = objective + obj.Q.'*xbar(:,i) + obj.R.'*ubar(:,i);          % Stage cost
            end
            objective = objective + obj.P.'*xbar(:,obj.N+1);        % Terminal cost
            
            % Constraints
            constraints = [];
            constraints = [constraints, xbar(:,1) == xbar_0, s(:,1) == s_0];    % Initialisation: for xbar and for s
            for i = 1:obj.N
                constraints = [constraints, s(:,i+1) == obj.Acl*s(:,i) + obj.B*c(:,i), xbar(:,i+1) == obj.A*xbar(:,i) + obj.B*ubar(:,i)];   % Dynamics
                constraints = [constraints, ubar(:,i) == obj.K*xbar(:,i) + c(:,i)];                                                         % Input equation
%                 constraints = [constraints, obj.F_tilde*s(:,i) + obj.G*c(:,i) <= 1 - gamma_i(:,i)];                                         % Constraints
            end
% implement constraints for i=1 separately!
for i = 2:obj.N
    constraints = [constraints, obj.F_tilde*s(:,i) + obj.G*c(:,i) <= 1 - gamma_i(:,i)]; 
end
for j = 1:obj.nc
    if norm(obj.G(j,:))>0
        constraints = [constraints, obj.F_tilde(j,:)*xbar_0 + obj.G(j,:)*c(:,1) <= 1];
    end
end

constraints = [constraints, obj.A_Zf*s(:,obj.N+1) <= obj.b_Zf];     % Terminal Set constraint
            
            
            % Settings
            % Yalmip parameters
            inputs = { xbar_0, s_0, [gamma_i] };
            outputs = { [c], [s], [ubar], [xbar] };
    %%% possibly add 'solver','mosek' to list
            if vis == true
                verbose_setting = 2;
            else
                verbose_setting = 0;
            end
% DEBUG
% verbose_setting = 2;       % CAN FORCE OUTPUTS
            options = sdpsettings('verbose',verbose_setting);
            
            % Create Yalmip object
            obj.solver = optimizer(constraints, objective, options, inputs, outputs);
        end
        
        
        
        function [u_0_opt, c_opt, s_opt, ubar_opt, xbar_opt, solution_error] = solve(obj, x_k, s_k, k)
            %%% CREATE TIGHTENING SEQUENCE FOR CURRENT TIMESTEP k %%%
            gamma_all_restlength = size(obj.gamma_all(:,1+k:end),2);
            if gamma_all_restlength <obj.N
                gamma_i(:,1:gamma_all_restlength) = obj.gamma_all(:,1+k:end);
                for ind=gamma_all_restlength+1:obj.N
                    gamma_i(:,ind) = obj.gamma_max;
                end
            else
                gamma_i = obj.gamma_all(:,1+k:obj.N+k);
            end
            
            %%% CALL YALMIP SOLVER %%%
            [results, solution_error] = obj.solver(x_k, s_k, gamma_i);
            
            % Extract relevant data
            c_opt = results{1};
            s_opt = results{2};
            ubar_opt = results{3};
            xbar_opt = results{4};
            
            if solution_error==0
%                 disp(yalmiperror(solution_error));
            else
                disp(yalmiperror(solution_error));
            end
            
            u_0_opt = obj.K*x_k + c_opt(:,1);
            
        end
        
        
        
        
        
    end
    
end