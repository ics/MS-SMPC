classdef multistep_SMPC_economic
% Simple multistep SMPC with economic cost
% Solved every Mmulti time steps; first Mmulti inputs are applied before
% the system is solved again
% Implementation with economic cost

    properties
        vis         % true/{false} - visualization + terminal outputs on/off
        N           % Prediction horizon
        Mmulti      % Multistep horizon
        
        nx          % State dimension
        nu          % Input dimension
        nw          % Disturbance dimension
        
        A           % Dynamics: state matrix
        B           % Dynamics: input matrix
        D           % Dynamics: disturbance matrix
        K           % Linear feedback controller
        Acl         % Closed-loop dynamics Acl = A+B*K
        
        Q           % State cost parameter
        R           % Input cost parameter
        P           % Terminal cost parameter
        
        % Probabilistic constraints 
        % of the form F_tilde*x + G*c <= 1 - beta_all
        F   
        F_tilde
        G
        Hx
        hx
        beta_all    % Constraint tightening terms for multistep prediction
        nc          % number of (combined) half-space constraints
        
        % Terminal set constraint
        % Z_f = {s | A_Zf * x <= b_Zf}
        A_Zf
        b_Zf
        
        % Potential hard input constraints
        % A_U0 * x_0 <= b_U0
        A_U0
        b_U0
        
        % Properties regarding the disturbance w
        w
        
        % Yalmip Object
        solver          % solver for k >= 1
        solver_init     % solver for k = 0
        
    end
    
    methods
        function obj = multistep_SMPC_economic(smpc_params,vis)
            switch nargin
                case 2
                    
                case 1
                    vis = false;
                otherwise
                    error('multistep_SMPC_economic: wrong number of arguments');
            end
            
            %%%% INTERNAL PARAMETERS %%%%
            obj.N = smpc_params.mpc.N;
            obj.Mmulti = smpc_params.mpc.Mmulti;
            
            obj.nx = smpc_params.sys.nx;
            obj.nu = smpc_params.sys.nu;
            obj.nw = smpc_params.sys.nw;
            
            obj.A = smpc_params.sys.A;
            obj.B = smpc_params.sys.B;
            obj.D = smpc_params.sys.D;
            obj.K = smpc_params.sys.K;
            obj.Acl = smpc_params.sys.Acl;
            
            obj.Q = smpc_params.cost.Q;
            obj.R = smpc_params.cost.R;
            obj.P = smpc_params.cost.P;
            
            obj.F = smpc_params.constraints.F;
            obj.F_tilde = smpc_params.constraints.F_tilde;
            obj.G = smpc_params.constraints.G;
            obj.Hx = smpc_params.constraints.Hx;
            obj.hx = smpc_params.constraints.hx;
            obj.beta_all = smpc_params.constraints.beta_all;
            obj.nc = size(obj.F,1);
            
            obj.A_Zf = smpc_params.constraints.A_Zf;
            obj.b_Zf = smpc_params.constraints.b_Zf;
            
            obj.A_U0 = smpc_params.constraints.A_U0;
            obj.b_U0 = smpc_params.constraints.b_U0;
            
            obj.w = smpc_params.w;
            
            
            %%%% SANITY CHCEK %%%%
            if obj.Mmulti >= obj.N
                error('multistep_SMPC_economic: multistep horizon must be smaller than MPC horizon!');
            end
            
            
            
            %%%% SOLVER %%%%
            % Optimization Variables
            xbar = sdpvar(obj.nx, obj.N+1, 'full');     % expected state
            s    = sdpvar(obj.nx, obj.Mmulti+1, 'full');% nominal state
            ubar = sdpvar(obj.nu, obj.N, 'full');       % expected input
            c    = sdpvar(obj.nu, obj.N, 'full');       % nominal input
            % Optimization Variables (to be fixed as parameters)
            xbar_0 = sdpvar(obj.nx, 1, 'full');     % initial expected state
            s_0    = sdpvar(obj.nx, 1, 'full');     % initial nominal state
            
%             % Objective
%             objective = 0;
%             for i = 1:obj.N         % Stage Cost
%                 objective = objective + xbar(:,i).'*obj.Q*xbar(:,i) + ubar(:,i).'*obj.R*ubar(:,i);
%             end
%             objective = objective + xbar(:,obj.N+1).'*obj.P*xbar(:,obj.N+1);    % Terminal Cost

% ECONOMIC COST OBJECTIVE
            objective = 0;
            for i = 1:obj.N
                objective = objective + obj.Q.'*xbar(:,i) + obj.R.'*ubar(:,i);  % Stage cost
            end
            objective = objective + obj.P.'*xbar(:,obj.N+1);    % Terminal cost

            % Constraints
            constraints = [];
            % initial state constraints
            constraints = [constraints, xbar(:,1) == xbar_0];
            constraints = [constraints, s(:,1) == s_0];
            % dynamics
            for i=1:obj.N
                constraints = [constraints, xbar(:,i+1) == obj.Acl*xbar(:,i) + obj.B*c(:,i)];
                constraints = [constraints, ubar(:,i) == obj.K*xbar(:,i) + c(:,i)]; 
            end
            for i=1:obj.Mmulti
                constraints = [constraints, s(:,i+1) == obj.Acl*s(:,i) + obj.B*c(:,i)];
            end
            % state and input constraints
            for i = obj.Mmulti+1:obj.N
                constraints = [constraints, obj.F_tilde*xbar(:,i) + obj.G*c(:,i) <= 1 - obj.beta_all(:,i)];
            end
            
% Implement constraints for i = 1 separately
for j = 1:obj.nc
    if norm(obj.G(j,:)) > 0
        constraints = [constraints, obj.F_tilde(j,:)*xbar_0 + obj.G(j,:)*c(:,1) <= 1];
    end
end
%             for i = 1:obj.Mmulti
            for  i = 2:obj.Mmulti
                constraints = [constraints, obj.F_tilde*s(:,i) + obj.G*c(:,i) <= 1 - obj.beta_all(:,obj.Mmulti+i)];
            end
            % terminal set constraint
            constraints = [constraints, obj.A_Zf*xbar(:,obj.N+1) <= obj.b_Zf];
            
            
            % Settings
            % Yalmip parameters
            inputs = { xbar_0, s_0};
            outputs = { [c], [s], [ubar], [xbar] };
    %%% possibly add 'solver','mosek' to list
            if vis == true
                verbose_setting = 2;
            else
                verbose_setting = 0;
            end
% DEBUG
% verbose_setting = 2       % CAN FORCE OUTPUTS
            options = sdpsettings('verbose',verbose_setting);
            
            % Create Yalmip object
            obj.solver = optimizer(constraints, objective, options, inputs, outputs);
            
            
            
            %%%% SOLVER_INIT %%%%
            % Optimization Variables
            xbar_init = sdpvar(obj.nx, obj.N+1, 'full');    % expected state
            ubar_init = sdpvar(obj.nu, obj.N, 'full');      % expected input
            c_init    = sdpvar(obj.nu, obj.N, 'full');      % nominal input
            % Optimization Variables (to be fixed as parameters)
            xbar_0_init = sdpvar(obj.nx, 1, 'full');    % initial expected state
            
            % Objective
%             objective_init = 0;
%             for i = 1:obj.N
%                 objective_init = objective_init + xbar_init(:,i).' * obj.Q * xbar_init(:,i) + ubar_init(:,i).' * obj.R * ubar_init(:,i);
%             end
%             objective_init = objective_init + xbar_init(:,obj.N+1).' * obj.P * xbar_init(:,obj.N+1);

% ECONOMIC COST OBJECTIVE
            objective_init = 0;
            for i = 1:obj.N
                objective_init = objective_init + obj.Q.'*xbar_init(:,i) + obj.R.'*ubar_init(:,i);  % Stage cost
            end
            objective_init = objective_init + obj.P.'*xbar_init(:,obj.N+1);    % Terminal cost
            
            % Constraints
            constraints_init = [];
            constraints_init = [constraints_init, xbar_init(:,1) == xbar_0_init];       % Initial State Constraint
            for i = 1:obj.N
                constraints_init = [constraints_init, xbar_init(:,i+1) == obj.Acl*xbar_init(:,i) + obj.B*c_init(:,i)];  % Dynamics
                constraints_init = [constraints_init, ubar_init(:,i) == obj.K*xbar_init(:,i) + c_init(:,i)];            % Input Equation
%                 constraints_init = [constraints_init, obj.F_tilde*xbar_init(:,i) + obj.G*c_init(:,i) <= 1 - obj.beta_all(:,i)]; % Constraints
            end
%Implement constraints on j = 0 separately
for j =1:obj.nc
    if norm(obj.G(j,:))>0
        constraints_init = [constraints_init, obj.F_tilde(j,:)*xbar_0_init + obj.G(j,:)*c_init(:,1) <= 1];
    end
end
for i = 2:obj.N
    constraints_init = [constraints_init, obj.F_tilde*xbar_init(:,i) + obj.G*c_init(:,i) <= 1 - obj.beta_all(:,i)]; % Constraints
end
    
    
            constraints_init = [constraints_init, obj.A_Zf*xbar_init(:,obj.N+1) <= obj.b_Zf];
            
           
            % Settings
            % Yalmip parameters
            inputs_init = xbar_0_init;
            outputs_init = {[c_init], [ubar_init], [xbar_init]};    % output variables
            options_init = options;
            
            % Create Yalmip object
            obj.solver_init = optimizer(constraints_init, objective_init, options_init, inputs_init, outputs_init);

        end
        
        
        
        
        function [c_k_opt, s_opt, ubar_opt, xbar_opt, solution_error] = solve(obj, x_k, xbar_last, t_k)
            
            if t_k == 0
                % Initial case k = 0
                % -> call solver_init
                [results, solution_error] = obj.solver_init(x_k);
                
                % Extract relevant data
                c_k_opt = results{1};
                ubar_opt = results{2};
                xbar_opt = results{3};
                
                % Check for issues
                if solution_error==0
    %                 disp(yalmiperror(solution_error));
                else
                    disp('solver_init');
                    disp(yalmiperror(solution_error));
                end
                
                % return s_opt = xbar_opt for simplicity (s_opt is not
                % computed for k = 0)
                s_opt = xbar_opt;
                
            else
                % Regular case k > 0
                % -> call solver
                [results, solution_error] = obj.solver(x_k, xbar_last);
                
                % Extract relevant data
                c_k_opt = results{1};
                s_opt = results{2};
                ubar_opt = results{3};
                xbar_opt = results{4};

                % Check for issues
                if solution_error==0
    %                 disp(yalmiperror(solution_error));
                else
                    disp('solver');
                    disp(yalmiperror(solution_error));
                end
            end
            
            
        end
        
    end
end
   