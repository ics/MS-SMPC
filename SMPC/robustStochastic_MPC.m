classdef robustStochastic_MPC
% robust stochastic MPC
% Implementation with (regular) quadratic cost

    properties
        vis         % true/{false} - visualization + terminal outputs on/off
        N           % Prediction horizon
        
        nx          % State dimension
        nu          % Input dimension
        nw          % Disturbance dimension
        
        A           % Dynamics: state matrix
        B           % Dynamics: input matrix
        D           % Dynamics: disturbance matrix
        K           % Linear feedback controller
        Acl         % Closed-loop dynamics Acl = A+B*K
        
        Q           % State cost parameter
        R           % Input cost parameter
        P           % Terminal cost parameter
        
        % Probabilistic constraints 
        % Z^(i) = {[s;c] | [F_tilde G] * [s;c] < 1 - beta_i(i)}
        F_tilde     
        G
        Hx
        hx
        beta_i
        
        % Terminal set constraint
        % Z_f = {s | A_Zf * s <= b_Zf}
        A_Zf
        b_Zf
        
        % Deterministic input constraint for i = 0
        % U^(0) = {c0 | A_U0 * c0 <= b_U0
        % if empty, no probabilistic input constraints
        A_U0
        b_U0
        det_input_constraint_bool
        
        % Properties regarding the disturbance w.
        w
        
        % Yalmip Object
        solver
        
    end
    
    methods
        function obj = robustStochastic_MPC(smpc_params, vis)
            switch nargin
                case 2
                    
                case 1
                    vis = false;
                otherwise
                    error('robustStochastic_MPC: wrong number of argunments');
            end
            %%%% INTERNAL PARAMETERS %%%%
            obj.vis = vis;
            
            %%%% LOAD DATA INTO THE CALSS %%%%
            obj.N = smpc_params.mpc.N;
            
            obj.nx = smpc_params.sys.nx;
            obj.nu = smpc_params.sys.nu;
            obj.nw = smpc_params.sys.nw;
            
            obj.A = smpc_params.sys.A;
            obj.B = smpc_params.sys.B;
            obj.D = smpc_params.sys.D;
            obj.K = smpc_params.sys.K;
            obj.Acl = obj.A + obj.B*obj.K;
            
            obj.Q = smpc_params.cost.Q;
            obj.R = smpc_params.cost.R;
            obj.P = smpc_params.cost.P;
            
            obj.F_tilde = smpc_params.constraints.F_tilde;
            obj.G = smpc_params.constraints.G;
            obj.Hx = smpc_params.constraints.Hx;
            obj.hx = smpc_params.constraints.hx;
            obj.beta_i = smpc_params.constraints.beta_i;
            
            obj.A_Zf = smpc_params.constraints.A_Zf;
            obj.b_Zf = smpc_params.constraints.b_Zf;
            
            obj.A_U0 = smpc_params.constraints.A_U0;
            obj.b_U0 = smpc_params.constraints.b_U0;
            obj.det_input_constraint_bool = smpc_params.constraints.det_input_constraint_bool;
            
            obj.w = smpc_params.w;

            
            %%%% CREATE YALMIP OBJECT %%%%
            % Optimization Variables
            c = sdpvar(obj.nu,obj.N,'full');
            s = sdpvar(obj.nx,obj.N+1,'full');
            x_k = sdpvar(obj.nx,1,'full');
            
            % Objective
            cost_matrix = [(obj.Q + obj.K.'*obj.R*obj.K), obj.K.'*obj.R; ...
                                             obj.R*obj.K, obj.R];
            objective = 0;
            objective = objective + s(:,obj.N+1).' * obj.P * s(:,obj.N+1);  % Terminal cost
            for j = 1:obj.N     % Stage cost
                objective = objective + [s(:,j); c(:,j)].'*cost_matrix*[s(:,j); c(:,j)];
            end
            
            % Constraints
            constraints = [];
            constraints = [constraints, s(:,1) == x_k];      % Initial state onstraint
%             if ~isempty(obj.A_U0)                           % Potential constraint on u0 (if deterministic)
            if obj.det_input_constraint_bool == true
                constraints = [constraints, obj.A_U0*c(:,1) <= obj.b_U0 - obj.A_U0*obj.K*s(:,1)];
%                 disp('DEBUG: Deterministic constraints on u0');         % DEBUG
            end
            constraints = [constraints, obj.A_Zf*s(:,obj.N+1) <= obj.b_Zf]; % Terminal state constraint
            for j = 2:obj.N     % Probabilistic Constraints
                constraints = [constraints, obj.F_tilde*s(:,j) + obj.G*c(:,j) <= 1-obj.beta_i(:,j-1)];
            end
            for j = 1:obj.N     % Dynamics
                constraints = [constraints, s(:,j+1) == obj.Acl*s(:,j) + obj.B*c(:,j)];
            end
            
            % Settings
%%% possibly add 'solver','mosek' to list
            if vis == true
                verbose_setting = 2;
            else
                verbose_setting = 0;
            end
% verbose_setting = 2;
            options = sdpsettings('verbose',verbose_setting);
            inputs = x_k;                           % input variables
            outputs = {[c], [s]};                   % output variables
            
            % Create Yalmip object
            obj.solver = optimizer(constraints, objective, options, inputs, outputs);
        end
        
        
        
        
        function [c_0_opt, s_i_opt, c_i_opt, solution_error] = solve(obj, x_k)
            % Solve the SMPC Problem using Yalmip
            [result,solution_error] = obj.solver(x_k);
            s_i_opt = result{2};
            c_i_opt = result{1};
            c_0_opt = c_i_opt(:,1);
            
            if solution_error==0
%                disp(yalmiperror(solution_error));
            else
                disp(yalmiperror(solution_error));
            end
        end


        
    end
    
end
    
        