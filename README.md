# **On stochastic MPC formulations with closed-loop guarantees - Analysis and a unifying framework** - Simulation Code

# Dependencies
- [YALMIP](https://yalmip.github.io/): for MPC problems
- [Mosek](https://www.mosek.com/): numerical solver for simulation of case 2 (Economic cost objective)



# Folders
- **Simulation Pipelines**: MAIN files for simulation; parameter files
- **SMPC**: MPC classes
- **Offline**: functions for computation of offline parameters
- **Utilities**: additional functions for simulation, etc.

Add all folders to your MATLAB path.



# Run simulations
**Case 1:** regulation problem with quadratic cost and individual tube controllers for each SMPC scheme. 
- Run `MAIN_quadratic_cost_different_K.m`
- As Mosek led to problems in this case, we did **not** use Mosek for this simultaion (please *remove* Mosek from your MATLAB path).

**Case 2:** economic cost problem
- Run `MAIN_economic_cost.m`
- Because quadprog led to feasibility issues in some cases, we used Mosek for this simulation (please *add* Mosek to your MATLAB path).

Each main file contains a full simulation pipeline with
- offline parameter computation,
- MPC setup,
- simulation,
- output of performance metrics,
- plotting of state trajectories, input trajectories, and constraint violation graphs.

**Pipeline settings** and **simulation parameters** can be adjusted in the main files. **System parameters** are set in `systemParams_4th_order_economic.m` for case 1, while in case 2, they are directly specified in the main file.



# SMPC classes
**Robust-stochastic MPC (RS-MPC):**

- Classes: 
 `robustStochastic_MPC.m` (quadratic cost) and `robustStochastic_MPC_economic.m` (economic cost)
 - Offline computation: `robustStochastic_MPC_offline.m`


**Indirect feedback SMPC (IF-SMPC):**

- Classes:
`indirectFeedback_SMPC_SF.m` and `indirectFeedback_SMPC_SF_economic.m`
- Offline computation: `indirectFeedback_SMPC_offline.`


**Multi-step SMPC (MS-SMPC, REMS-SMPC, or re-evaluating multistep SMPC):**

- Classes: `multistep_reeval_SMPC.m` and `multistep_reeval_SMPC_economic.m`
- Offline computation: `multistep_reeval_SMPC_offline.m`



**simple multi-step SMPC (simple MS-SMPC)**:
- not used in the paper
- simplified multi-step SMPC method where the MPC problem is only re-solved every $M$ steps, and the first $M$ inputs are applied to the system
- Classes: `multistep_SMPC.m` and `multistep_SMPC_economic.m`
- Offline computation: `multistep_SMPC_offline.m`
