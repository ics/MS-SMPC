function [x_traj_rems, c_traj_rems, u_traj_rems, s_nom_traj_rems,avgcost,violations_traj_rems] = trajectory_rems_smpc(rems_smpc_obj,x_0,N_sim,w_traj)
% Simulation of (regular) MS-SMPC over N_sim steps, starting from x_0 with
% disturbance trajectory w_traj, using MS-SMPC object rems_smpc_obj created
% beforehand.


% Obtain parameters
dist = rems_smpc_obj.w.dist;
nx = rems_smpc_obj.nx;
nu = rems_smpc_obj.nu;
nw = rems_smpc_obj.nw;
A = rems_smpc_obj.A;
B = rems_smpc_obj.B;
D = rems_smpc_obj.D;
K = rems_smpc_obj.K;
Acl = rems_smpc_obj.Acl;
Hx = rems_smpc_obj.Hx;
hx = rems_smpc_obj.hx;
Hu = rems_smpc_obj.A_U0;
hu = rems_smpc_obj.b_U0;
no_state_constr = length(hx);

Q = rems_smpc_obj.Q;
R = rems_smpc_obj.R;

Mmulti = rems_smpc_obj.Mmulti;

% Setup
x_traj_rems = [];
s_nom_traj_rems = [];
x_nom_traj_rems = [];
c_traj_rems = [];
u_traj_rems = [];
cost_counter_rems = 0;
violations_traj_rems = [];

x_temp_rems = x_0;
x_nom_temp_rems = x_0; % for k < 0, s_nom_temp = x_nom_temp
x_traj_rems = [x_traj_rems, x_temp_rems];
k_rems = 0;

violations_temp_rems = [];
for idx_constr = 1:no_state_constr
    if Hx(idx_constr,:)*x_temp_rems <= hx(idx_constr)
        violations_temp_rems(idx_constr) = 0;
    else
        violations_temp_rems(idx_constr) = 1;
    end
end
violations_traj_rems = [violations_traj_rems, violations_temp_rems.'];


% Iterate
for i = 1:N_sim
    w_temp = w_traj(:,i);
    
    pred_step_rems = mod(k_rems,Mmulti);
    if k_rems < Mmulti
        M_k_rems = k_rems;
    else
        M_k_rems = Mmulti + pred_step_rems;
    end

    % prepare nominal states
    if pred_step_rems == 0
        s_nom_temp_rems = x_nom_temp_rems;
        x_nom_temp_rems = x_temp_rems;
    end
    x_nom_traj_rems = [x_nom_traj_rems, x_nom_temp_rems];
    s_nom_traj_rems = [s_nom_traj_rems, s_nom_temp_rems];

    % obtain input
    [c_opt_rems,s_opt_rems,ubar_opt_rems,xbar_opt_rems,x_cost_opt_rems, u_cost_opt_rems,error_code_rems] = rems_smpc_obj.solve(x_temp_rems, x_nom_temp_rems,s_nom_temp_rems,k_rems);
    if error_code_rems~=0
        disp(['(re-evaluating) Multistep SMPC: pred_step_rems = ',num2str(pred_step_rems)]);
        disp(['k_rems = ',num2str(k_rems)]);
        disp(['i = ',num2str(i)]);
        error(yalmiperror(error_code_rems));
    end
    c_temp_rems = c_opt_rems(:,1);
    u_temp_rems = K*x_temp_rems + c_temp_rems;
    
    % cost update
    cost_counter_rems = cost_counter_rems + x_temp_rems.'*Q*x_temp_rems + u_temp_rems.'*R*u_temp_rems;

    % system update
    x_temp_rems = A*x_temp_rems + B*u_temp_rems + D*w_temp;
    x_nom_temp_rems = Acl*x_nom_temp_rems + B*c_temp_rems;
    s_nom_temp_rems = Acl*s_nom_temp_rems + B*c_temp_rems;
    k_rems = k_rems + 1;

    x_traj_rems = [x_traj_rems, x_temp_rems];
    u_traj_rems = [u_traj_rems, u_temp_rems];
    c_traj_rems = [c_traj_rems, c_temp_rems];
    
    violations_temp_rems = [];
    for idx_constr = 1:no_state_constr
        if Hx(idx_constr,:)*x_temp_rems <= hx(idx_constr)
            violations_temp_rems(idx_constr) = 0;
        else
            violations_temp_rems(idx_constr) = 1;
        end
    end
    violations_traj_rems = [violations_traj_rems, violations_temp_rems.'];
end

% Compute average cost
avgcost = cost_counter_rems/N_sim;

if false
% Visualize Test Simulation
figure();
sgtitle(['REMS-SMPC (K_{ms}): 4th-order Integrator Simulation Results for ',num2str(N_sim), ' steps, x(0) = [',num2str(x_0(1)),';',num2str(x_0(2)),';',num2str(x_0(3)),';',num2str(x_0(4)),']']);

subplot(2,1,1);
hold on
plot(0:N_sim,x_traj_rems(1,:),'k.-');
plot(0:N_sim-1,s_nom_traj_rems(1,:),'-','color',[0.5 0.5 0.5]);
yline(hx(1)/Hx(1,1),'r','linewidth',1);
grid on
legend('x','s','State Constraints');
title(['State Trajectory (1st dim.); l_{avg} = ',num2str(avgcost)]);
xlabel('k');
ylabel('x_1');
axis padded

subplot(2,1,2);
hold on;
plot(0:N_sim-1,u_traj_rems,'k.-');
yline(hu(1)/Hu(1),'r','linewidth',1);
yline(hu(2)/Hu(2),'r','linewidth',1);
grid on;
legend('u','Input Constraints');
title('Input Trajectory');
xlabel('k');
ylabel('u');
axis padded;
end
