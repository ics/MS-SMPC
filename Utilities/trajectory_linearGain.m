function [x_traj_tube, u_traj_tube,avgcost,violations_traj_tube] = trajectory_linearGain(K_tube,x_0,N_sim,sys_params,fig_no,w_traj)
% Simulation of linear control gain K_tube over N_sim steps, starting from
% x_0 with disturbance trajectory w_traj. Requires that a figure number is
% set in fig_no, and requires system parameters to be handed over in
% sys_params.

% Obtain parameters
A = sys_params.A;
B = sys_params.B;
D = sys_params.D;
dist = sys_params.dist;
nw = sys_params.nw;
Hx = sys_params.Hx;
hx = sys_params.hx;
Hu = sys_params.Hu;
hu = sys_params.hu;
Q = sys_params.Q;
R = sys_params.R;
no_state_constr = length(hx);
Acl = A+B*K_tube;

% Setup
x_traj_tube = [];
u_traj_tube = [];
cost_counter_tube = 0;
violations_traj_tube = [];

x_temp_tube = x_0;
x_traj_tube = x_0;
violations_temp_tube = [];
for idx_constr = 1:no_state_constr
    if Hx(idx_constr,:)*x_temp_tube <= hx(idx_constr)
        violations_temp_tube(idx_constr) = 0;
    else
        violations_temp_tube(idx_constr) = 1;
    end
end
violations_traj_tube = [violations_traj_tube, violations_temp_tube.'];


% Iterate
for i = 1:N_sim
    w_temp = w_traj(:,i);
    u_temp_tube = K_tube*x_temp_tube;
    
    cost_counter_tube = cost_counter_tube + x_temp_tube.' * Q * x_temp_tube + u_temp_tube.' * R * u_temp_tube;
    x_temp_tube = Acl*x_temp_tube + D*w_temp;
    
    u_traj_tube = [u_traj_tube, u_temp_tube];
    x_traj_tube=[x_traj_tube,x_temp_tube];
    violations_temp_tube = [];
    for idx_constr = 1:no_state_constr
        if Hx(idx_constr,:)*x_temp_tube <= hx(idx_constr)
            violations_temp_tube(idx_constr) = 0;
        else
            violations_temp_tube(idx_constr) = 1;
        end
    end
    violations_traj_tube = [violations_traj_tube, violations_temp_tube.'];
end

% Compute average cost:
 avgcost = cost_counter_tube/N_sim;
 
 
if false
% Visualize Test Simulation
figure(fig_no);
sgtitle(['Linear Controller K: 4th-order Integrator Simulation Results for ',num2str(N_sim), ' steps, x(0) = [',num2str(x_0(1)),';',num2str(x_0(2)),';',num2str(x_0(3)),';',num2str(x_0(4)),']']);

subplot(2,1,1);
hold on
plot(0:N_sim,x_traj_tube(1,:),'k.-');
yline(hx(1)/Hx(1,1),'r','linewidth',1);
grid on
legend('x','State Constraints');
title(['State Trajectory (1st dim.); l_{avg} = ',num2str(avgcost)]);
xlabel('k');
ylabel('x_1');
axis padded

subplot(2,1,2);
hold on;
plot(0:N_sim-1,u_traj_tube,'k.-');
yline(hu(1)/Hu(1),'r','linewidth',1);
yline(hu(2)/Hu(2),'r','linewidth',1);
grid on;
legend('u','Input Constraints');
title('Input Trajectory');
xlabel('k');
ylabel('u');
axis padded;
end
