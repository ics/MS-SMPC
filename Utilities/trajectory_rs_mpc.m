function [x_traj_rs, c_traj_rs, u_traj_rs,w_traj, avgcost,violations_traj_rs] = trajectory_rs_mpc(rs_mpc_obj,x_0,N_sim,w_traj)
% Simulation of RS-MPC over N_sim steps, starting from x_0 with disturbance
% trajectory w_traj, using RS-MPC object rs_mpc_obj created beforehand.

% Obtain parameters
dist = rs_mpc_obj.w.dist;
nx = rs_mpc_obj.nx;
nu = rs_mpc_obj.nu;
nw = rs_mpc_obj.nw;
A = rs_mpc_obj.A;
B = rs_mpc_obj.B;
D = rs_mpc_obj.D;
K = rs_mpc_obj.K;
Acl = rs_mpc_obj.Acl;
Hx = rs_mpc_obj.Hx;
hx = rs_mpc_obj.hx;
Hu = rs_mpc_obj.A_U0;
hu = rs_mpc_obj.b_U0;
no_state_constr = length(hx);

Q = rs_mpc_obj.Q;
R = rs_mpc_obj.R;

% Setup
x_traj_rs = [];
c_traj_rs = [];
u_traj_rs = [];
cost_counter_rs = 0;
violations_traj_rs = [];

x_temp_rs = x_0;
x_traj_rs = x_0;
violations_temp_rs = [];
for idx_constr = 1:no_state_constr
    if Hx(idx_constr,:)*x_temp_rs <= hx(idx_constr)
        violations_temp_rs(idx_constr) = 0;
    else
        violations_temp_rs(idx_constr) = 1;
    end
end
violations_traj_rs = [violations_traj_rs, violations_temp_rs.'];


% Iterate
for i = 1:N_sim
    w_temp = w_traj(:,i);
    
    [c_opt_rs,s_opt_temp_rs,c_opt_temp_rs,error_code_rs] = rs_mpc_obj.solve(x_temp_rs);
    if error_code_rs~=0
        disp(['Robust Stochastic MPC: i = ',num2str(i)]);
        error(yalmiperror(error_code_rs));
    end
    
     u_temp_rs = K*x_temp_rs + c_opt_rs;
     cost_counter_rs = cost_counter_rs + x_temp_rs.' * Q * x_temp_rs + u_temp_rs.' * R * u_temp_rs;
    
    c_traj_rs = [c_traj_rs, c_opt_rs];
    u_traj_rs = [u_traj_rs, K*x_temp_rs + c_opt_rs];
    

    x_temp_rs = Acl*x_temp_rs + B*c_opt_rs + D*w_temp;
    x_traj_rs=[x_traj_rs,x_temp_rs];
                    violations_temp_rs = [];
                    for idx_constr = 1:no_state_constr
                        if Hx(idx_constr,:)*x_temp_rs <= hx(idx_constr)
                            violations_temp_rs(idx_constr) = 0;
                        else
                            violations_temp_rs(idx_constr) = 1;
                        end
                    end
                    violations_traj_rs = [violations_traj_rs, violations_temp_rs.'];
end

% Compute average cost:
 avgcost = cost_counter_rs/N_sim;
 
if false
% Visualize Test Simulation
figure();
sgtitle(['RS-MPC (K_{sub}): 4th-order Integrator Simulation Results for ',num2str(N_sim), ' steps, x(0) = [',num2str(x_0(1)),';',num2str(x_0(2)),';',num2str(x_0(3)),';',num2str(x_0(4)),']']);

subplot(2,1,1);
hold on
plot(0:N_sim,x_traj_rs(1,:),'k.-');
yline(hx(1)/Hx(1,1),'r','linewidth',1);
grid on
legend('x','State Constraints');
title(['State Trajectory (1st dim.); l_{avg} = ',num2str(avgcost)]);
xlabel('k');
ylabel('x_1');
axis padded

subplot(2,1,2);
hold on;
plot(0:N_sim-1,u_traj_rs,'k.-');
yline(hu(1)/Hu(1),'r','linewidth',1);
yline(hu(2)/Hu(2),'r','linewidth',1);
grid on;
legend('u','Input Constraints');
title('Input Trajectory');
xlabel('k');
ylabel('u');
axis padded;
end
