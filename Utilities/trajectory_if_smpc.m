function [x_traj_ifsf, s_traj_ifsf, u_traj_ifsf, c_traj_ifsf, u_nom_traj_ifsf,w_traj,avgcost,violations_traj_ifsf] = trajectory_if_smpc(if_smpc_obj, x_0, N_sim,w_traj)
% Simulation of IF-SMPC over N_sim steps, starting from x_0 with
% disturbance trajectory w_traj, using IF-SMPC object if_smpc_obj created
% beforehand.


% Obtain parameters
dist = if_smpc_obj.w.dist;
nx = if_smpc_obj.nx;
nu = if_smpc_obj.nu;
nw = if_smpc_obj.nw;
A = if_smpc_obj.A;
B = if_smpc_obj.B;
D = if_smpc_obj.D;
K = if_smpc_obj.K;
Acl = if_smpc_obj.Acl;
Hx = if_smpc_obj.Hx;
hx = if_smpc_obj.hx;
no_state_constr = length(hx);

Q = if_smpc_obj.Q;
R = if_smpc_obj.R;

% Setup
x_traj_ifsf = [];
s_traj_ifsf = [];
c_traj_ifsf = [];
u_traj_ifsf = [];
u_nom_traj_ifsf = [];
cost_counter_ifsf = 0;
violations_traj_ifsf = [];

% Step k = 0
k_ifsf = 0;
x_temp_ifsf = x_0;
s_temp_ifsf = x_0;
x_traj_ifsf = x_0;
s_traj_ifsf = x_0;
violations_temp_ifsf = [];
    for idx_constr = 1:no_state_constr
        if Hx(idx_constr,:)*x_temp_ifsf <= hx(idx_constr)
            violations_temp_ifsf(idx_constr) = 0;
        else
            violations_temp_ifsf(idx_constr) = 1;
        end
    end
    violations_traj_ifsf = [violations_traj_ifsf, violations_temp_ifsf.'];

% Iterate
for i=1:N_sim
    w_temp = w_traj(:,i);
    
    [u_0_opt_ifsf, c_opt_ifsf, s_opt_ifsf, ubar_opt_temp_ifsf, xbar_opt_temp_ifsf, error_code_ifsf] = if_smpc_obj.solve(x_temp_ifsf, s_temp_ifsf, k_ifsf);
    if error_code_ifsf~=0
        disp(['indirect Feedback SMPC: i=',num2str(i)]);
        error(yalmiperror(error_code_ifsf));
    end
    
    cost_counter_ifsf = cost_counter_ifsf + x_temp_ifsf.' * Q * x_temp_ifsf + u_0_opt_ifsf.' * R * u_0_opt_ifsf;
    x_temp_ifsf = A*x_temp_ifsf + B*u_0_opt_ifsf + D*w_temp;
    u_nom_temp_ifsf = K*s_temp_ifsf + c_opt_ifsf(:,1);
%     s_temp_ifsf = s_opt_ifsf(:,2);
    s_temp_ifsf = Acl*s_temp_ifsf + B*c_opt_ifsf(:,1);
    k_ifsf = k_ifsf + 1;
    
    x_traj_ifsf = [x_traj_ifsf, x_temp_ifsf];
    s_traj_ifsf = [s_traj_ifsf, s_temp_ifsf];
    u_traj_ifsf = [u_traj_ifsf, u_0_opt_ifsf];
    c_traj_ifsf = [c_traj_ifsf, c_opt_ifsf(:,1)];
    u_nom_traj_ifsf = [u_nom_traj_ifsf, u_nom_temp_ifsf];
    violations_temp_ifsf = [];
    for idx_constr = 1:no_state_constr
        if Hx(idx_constr,:)*x_temp_ifsf <= hx(idx_constr)
            violations_temp_ifsf(idx_constr) = 0;
        else
            violations_temp_ifsf(idx_constr) = 1;
        end
    end
    violations_traj_ifsf = [violations_traj_ifsf, violations_temp_ifsf.'];
end

% Compute average cost:
avgcost = cost_counter_ifsf/N_sim;

if false  %deactive plots for now
% Visualize Test Simulation
figure();
sgtitle(['IF-SMPC (K_{LQR}): 4th-order Integrator Simulation Results for ',num2str(N_sim), ' steps, x(0) = [',num2str(x_0(1)),';',num2str(x_0(2)),';',num2str(x_0(3)),';',num2str(x_0(4)),']']);

subplot(2,1,1);
hold on
plot(0:N_sim,x_traj_ifsf(1,:),'.-','color',[0 0.4470 0.7410]);
plot(0:N_sim,s_traj_ifsf(1,:),'-','color',[0.5 0.5 0.5]);
yline(hx(1)/Hx(1,1),'r','linewidth',1);
grid on
legend('x','s','State Constraints');
title(['State Trajectory (1st dim.); l_{avg} = ',num2str(avgcost)]);
xlabel('k');
ylabel('x_1');
axis padded

subplot(2,1,2);
hold on;
plot(0:N_sim-1,u_traj_ifsf,'.-','color',[0 0.4470 0.7410]);
% yline(hu(1)/Hu(1),'r','linewidth',1);
% yline(hu(2)/Hu(2),'r','linewidth',1);
grid on;
legend('u');%,'Input Constraints');
title('Input Trajectory');
xlabel('k');
ylabel('u');
axis padded;
end