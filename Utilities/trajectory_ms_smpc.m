function [x_traj_ms, c_traj_ms, u_traj_ms, avgcost,violations_traj_ms] = trajectory_ms_smpc(ms_smpc_obj,x_0,N_sim,w_traj)
% Simulation of simple Multistep SMPC over N_sim steps, starting from x_0
% with disturbance trajectory w_traj, using simple Multistep SMPC object
% ms_smpc_obj created beforehand.

% Obtain parameters
dist = ms_smpc_obj.w.dist;
nx = ms_smpc_obj.nx;
nu = ms_smpc_obj.nu;
nw = ms_smpc_obj.nw;
A = ms_smpc_obj.A;
B = ms_smpc_obj.B;
D = ms_smpc_obj.D;
K = ms_smpc_obj.K;
Acl = ms_smpc_obj.Acl;
Hx = ms_smpc_obj.Hx;
hx = ms_smpc_obj.hx;
Hu = ms_smpc_obj.A_U0;
hu = ms_smpc_obj.b_U0;
no_state_constr = length(hx);

Q = ms_smpc_obj.Q;
R = ms_smpc_obj.R;

Mmulti = ms_smpc_obj.Mmulti;

% Setup
x_traj_ms = [];
c_traj_ms = [];
u_traj_ms = [];
cost_counter_ms = 0;
violations_traj_ms = [];

x_temp_ms = x_0;
x_last_ms = x_0;                        % Initialisation of x_last_ms is irrelevant, as not needed for initial solution
x_traj_ms = [x_traj_ms, x_temp_ms];
tk_ms = 0;

violations_temp_ms = [];
for idx_constr = 1:no_state_constr
    if Hx(idx_constr,:)*x_temp_ms <= hx(idx_constr)
        violations_temp_ms(idx_constr) = 0;
    else
        violations_temp_ms(idx_constr) = 1;
    end
end
violations_traj_ms = [violations_traj_ms, violations_temp_ms.'];


% Iterate
for i = 1:N_sim
    w_temp = w_traj(:,i);
    
    pred_step_ms = mod(tk_ms,Mmulti);
    % obtain input
    if pred_step_ms == 0
        % Re-solve the SMPC problem
        [c_k_opt_ms, ~, ~, xbar_opt_ms, error_code_ms] = ms_smpc_obj.solve(x_temp_ms, x_last_ms, tk_ms);
        if error_code_ms~=0
            disp(['simple Multistep SMPC: i = ',num2str(i)]);
            error(yalmiperror(error_code_ms));
        end
    % else: keep using previous sequence    
    end
    c_temp_ms = c_k_opt_ms(:,pred_step_ms+1);
    u_temp_ms = K*x_temp_ms + c_temp_ms;
    
    % cost update
    cost_counter_ms = cost_counter_ms + x_temp_ms.' * Q * x_temp_ms + u_temp_ms.' * R * u_temp_ms;

    % system update
    x_temp_ms = A*x_temp_ms + B*u_temp_ms + D*w_temp;
    x_last_ms = xbar_opt_ms(:,Mmulti+1);
    tk_ms = tk_ms + 1;

    x_traj_ms = [x_traj_ms, x_temp_ms];
    c_traj_ms = [c_traj_ms, c_temp_ms];
    u_traj_ms = [u_traj_ms, u_temp_ms];
    
    violations_temp_ms = [];
    for idx_constr = 1:no_state_constr
        if Hx(idx_constr,:)*x_temp_ms <= hx(idx_constr)
            violations_temp_ms(idx_constr) = 0;
        else
            violations_temp_ms(idx_constr) = 1;
        end
    end
    violations_traj_ms = [violations_traj_ms, violations_temp_ms.'];
end

% Compute average cost:
avgcost = cost_counter_ms/N_sim;

if false
% Visualize Test Simulation
figure();
sgtitle(['MS-SMPC (K_{ms}): 4th-order Integrator Simulation Results for ',num2str(N_sim), ' steps, x(0) = [',num2str(x_0(1)),';',num2str(x_0(2)),';',num2str(x_0(3)),';',num2str(x_0(4)),']']);

subplot(2,1,1);
hold on
plot(0:N_sim,x_traj_ms(1,:),'k.-');
yline(hx(1)/Hx(1,1),'r','linewidth',1);
grid on
legend('x','State Constraints');
title(['State Trajectory (1st dim.); l_{avg} = ',num2str(avgcost)]);
xlabel('k');
ylabel('x_1');
axis padded

subplot(2,1,2);
hold on;
plot(0:N_sim-1,u_traj_ms,'k.-');
yline(hu(1)/Hu(1),'r','linewidth',1);
yline(hu(2)/Hu(2),'r','linewidth',1);
grid on;
legend('u','Input Constraints');
title('Input Trajectory');
xlabel('k');
ylabel('u');
axis padded;
end




    
