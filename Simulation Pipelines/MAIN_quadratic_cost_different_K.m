%% SMPC pipeline - 4TH-ORDER INTEGRATOR WITH QUADRATIC COST - DIFFERENT TUBE CONTROLLERS
% Simulation Pipeline for Case 1


% Remove Mosek from MATLAB path.

close all
clear
clc

% Simulation Settings follow below

%% Pipeline Settings
do_logging = 0;                 % if true, terminal output and workspace will be logged/saved

do_lqr_tightening = 1;          % compute exact stochastic constraint tightening with LQR
do_if_sim = 1;                  % do IF-SMPC simulation

do_rs_tightening = 1;           % compute robust stochastic constraint tightening with suboptimal Ksub
do_rs_sim = 1;                  % do RS-MPC simulation

do_multistep_tightening = 1;    % compute multi-step constraint tightening with suboptimal Kms
do_ms_sim = 1;                  % do (regular) MS-SMPC simulation
do_simple_ms_sim = 0;           % do simple multi-step SMPC simulation

do_tube_sim = 1;                % simulate linear tube controllers (unconstrained)


%% Start Logging
current_time = datetime('now');
t_string = replace(string(current_time),':','-');
% start logging the command line outputs
workspace_path = strcat('Workspace ',t_string,'4th.mat');
cmd_path = strcat('Command Window ',t_string,'4th.txt');
if do_logging == true
    diary(cmd_path)
    disp('START LOGGING');
end



disp(" ");
disp(" ");
disp("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
disp("%                                                                      %");
disp("%           4TH-ORDER INTEGRATOR - Different Tube Controllers          %");
disp("%                                                                      %");
disp("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
disp(" ");


%% Simulation Parameters
N = 75;             % MPC horizon length
N_sim = 300;        % Simulation length
Mmulti = 35;        % Multistep horizon length
Nbar = 300;         % = \bar{k} in paper: largest computed gamma_k
Nrest = 0;          % (internal parameter)
x_0 = 0*[1;0;0;0];  % initial state
M = 1000;           % Number of simulation realisations
p = [0.7,1,1];      % probability level vector
q11 = 1.32;         % Q_{11}
q11_ms = 16.6;      % Q_{MS,11}
q11_rs = 740;       % Q_{RS,11}


%% System Parameters
disp('PARAMETERS');
% GENERAL SOLVER SETTINGS
linprog_options = optimoptions('linprog','Display','none');

% STOCHASTIC PARAMETERS
Ns = 1e6;                    % no. of samples for scenario approach
dist_lower_bound = -4;       % Paramters for uniform distribution
dist_upper_bound = 4;
beta_conf = 1e-4;                    % parameter for confidence = 1-beta
pd = makedist('Uniform',dist_lower_bound, dist_upper_bound);


% SYSTEM
nx = 4;
nu = 1;
nw = 1;

Ts = 0.1;
A = [1, Ts, Ts^2/2, Ts^3/6; ...
     0, 1, Ts, Ts^2/2; ...
     0, 0, 1, Ts; ...
     0, 0, 0, 1];
B = [Ts^4/24; ...
     Ts^3/6; ...
     Ts^2/2; ...
     Ts];
D = B; 


% COST
Q = diag([q11, 0, 0, 0]);
R = 0.1;
% STATE CONSTRAINTS
Hx = 1/0.1 * [1, 0, 0, 0];
hx = 1;
% INPUT CONSTRAINTS
Hu = 1/20 * [1; -1];
hu = [1;1];
det_input_constraint = true;
% AUGMENTED CONSTRAINT MATRIX
F = [Hx;...
     zeros(size(Hu,1),nx)];
G = [zeros(size(Hx,1),nu);...
     Hu];

% Some things on w
w_max = ones(nw,1)*max(abs([dist_lower_bound,dist_upper_bound]));
w_bar = norm(D*w_max,2);
sigma_squared_w = (dist_upper_bound - dist_lower_bound)^2 / 12;
Sigma_w = sigma_squared_w * D * D.';
% Obtain distribution support matrices Hw, hw of W
Hw = zeros(2*nw,nw);
hw = [];
for l = 1:nw
    Hw(2*l-1,l) = 1;
    Hw(2*l,l) = -1;
    hw = [hw; dist_upper_bound; -dist_lower_bound];
end

no_state_constr = length(hx);

W_all = random(pd,M*nw,N_sim);


%% Controller Setup
%LQR controller
disp('LQR CONTROLLER');
[K_lqr,P_lqr,~] = dlqr(A,B,Q,R);
K_lqr=-K_lqr;
Acl_lqr = A+B*K_lqr;
avgcost_lqr = trace(P_lqr*Sigma_w)

%suboptimal controller for RS-MPC
disp('SUBOPTIMAL CONTROLLER');
Qsub = diag([q11_rs 0 0 0])
[K_sub,~,~] = dlqr(A,B,Qsub,R);
K_sub = -K_sub;     % dlqr determines K s.t. A-B*K stable. I use the notation A+B*K stable
Acl_sub = A+B*K_sub;
P_sub = dlyap((A+B*K_sub)',Q + K_sub.'*R*K_sub);
avgcost_sub = trace(P_sub*Sigma_w)

%Multistep tube controller
disp('MULTISTEP: TUBE CONTROLLER - currently just Ksub');
Q_ms = diag([q11_ms 0 0 0])
[K_ms,~,~] = dlqr(A,B,Q_ms,R);
K_ms = -K_ms;     % dlqr determines K s.t. A-B*K stable. I use the notation A+B*K stable
Acl_ms = A+B*K_ms;
P_ms = dlyap((A+B*K_ms)',Q + K_ms.'*R*K_ms);
avgcost_Kms = trace(P_ms*Sigma_w)


disp(' ');
disp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%');




%% LQR tightening etc.
if do_lqr_tightening == true
    disp('Exact stochastic tightening with LQR');
    F_tild_lqr = F + G*K_lqr;
    % Compute gamma_all_lqr
    tic
    gamma_all_lqr = exactTight_2(p,Nbar,Ns,beta_conf,Acl_lqr,F_tild_lqr,D,pd,dist_lower_bound,dist_upper_bound);
    for i = 2:size(gamma_all_lqr,2)
        for j = 1:size(gamma_all_lqr,1)
            gamma_all_lqr(j,i) = max(gamma_all_lqr(j,i),gamma_all_lqr(j,i-1));
        end
    end
    toc

    % Compute gamma_max_lqr
    gamma_max_lqr = compute_gamma_max(gamma_all_lqr(:,Nbar+1),Nbar, Acl_lqr,F_tild_lqr,w_bar);
    % Check if all tightening terms are smaller/equal 1
    if max(gamma_max_lqr) > 1
        disp(num2str(gamma_max_lqr));
        warning("exact stochastic tightening (LQR): max(gamma_max) > 1 --> Tightening resulting in empty set");
    end


    % Compute robust stochastic tightening with LQR - for compariaon
    disp('Robust stochastic tightening with LQR');
    tic
    gamma_1_lqr_test = gamma_all_lqr(:,2);
    beta_all_lqr_test = zeros(length(gamma_1_lqr_test), N-1);
    for j = 1:N-1
        beta_all_lqr_test(:,j) = robustStochTight(j,gamma_1_lqr_test,F_tild_lqr,Acl_lqr,D,dist_lower_bound,dist_upper_bound);
    end
    beta_all_lqr_test = [zeros(size(beta_all_lqr_test,1),1), beta_all_lqr_test];
    if max(max(beta_all_lqr_test)) > 1
        disp([num2str(max(max(beta_all_lqr_test)))]);
        disp("robust stochastic tightening (LQR): max(max(beta_i)) > 1");
    end
    toc
    
    
    % Compute multistep tightening with LQR - for comparison
    disp('Multistep tightening with LQR');
    beta_tilde_all_lqr_test = multistepTight(p,N,Mmulti,Ns,beta_conf,Acl_lqr,F_tild_lqr,D,pd,dist_lower_bound,dist_upper_bound);

    if true
    % Compute robust tightening
        disp('Robust Tightening with LQR');
        tic
        delta_all_lqr_test = [];
        for i = 1:Nrest
            delta_temp = robustTight(i,F_tild_lqr,Acl_lqr,D,dist_lower_bound,dist_upper_bound);
            delta_all_lqr_test = [delta_all_lqr_test, delta_temp];
        end
        delta_all_lqr_test = [zeros(size(delta_all_lqr_test,1),1),delta_all_lqr_test];
        toc
    end
    
    disp(' ');
    disp(['For LQR: gamma_max on x_1: ',num2str(gamma_max_lqr(1))]);
%     disp(['For LQR: beta_max on x_1: ',num2str(max(beta_all_lqr_test(1,:)))]);
%     disp(['For LQR: beta_tilde_max on x_1: ',num2str(max(beta_tilde_all_lqr_test(1,:)))]);
end



%% Testsim: IF-SMPC with LQR
if do_if_sim == true
    disp(' ');
    disp('SIMULATION: IF-SMPC with LQR');
    %% Terminal Set
    disp('Compute Terminal Set');
    % Compute terminal set for IF-SMPC. 
    if max(gamma_max_lqr) > 1
        disp(num2str(gamma_max_lqr));
        error("max(gamma_max) > 1 --> Tightening resulting in empty set");
    end
    [A_Zf_IF, b_Zf_IF, nu_IF] = computeTerminalSet_IF_SMPC(Acl_lqr,F_tild_lqr,gamma_max_lqr);

    % Create Parameter Struct
    disp('Create Parameter Struct and SMPC object');
    if_smpc_params_lqr.mpc.N = N;
    if_smpc_params_lqr.sys.A = A;
    if_smpc_params_lqr.sys.B = B;
    if_smpc_params_lqr.sys.D = D;
    if_smpc_params_lqr.sys.nx = nx;
    if_smpc_params_lqr.sys.nu = nu;
    if_smpc_params_lqr.sys.nw = nw;
    if_smpc_params_lqr.sys.K = K_lqr;
    if_smpc_params_lqr.sys.Acl = Acl_lqr;
    if_smpc_params_lqr.cost.Q = Q;
    if_smpc_params_lqr.cost.R = R;
    if_smpc_params_lqr.cost.P = P_lqr;
    if_smpc_params_lqr.constraints.F = F;
    if_smpc_params_lqr.constraints.G = G;
    if_smpc_params_lqr.constraints.F_tilde = F_tild_lqr;
    if_smpc_params_lqr.constraints.Hx = Hx;
    if_smpc_params_lqr.constraints.hx = hx;
    if_smpc_params_lqr.constraints.gamma_all = gamma_all_lqr;
    if_smpc_params_lqr.constraints.gamma_max = gamma_max_lqr;
    if_smpc_params_lqr.constraints.Nbar = Nbar;
    if_smpc_params_lqr.constraints.A_Zf = A_Zf_IF;
    if_smpc_params_lqr.constraints.b_Zf = b_Zf_IF;
    if_smpc_params_lqr.w.dist_lower_bound = dist_lower_bound;
    if_smpc_params_lqr.w.dist_upper_bound = dist_upper_bound;
    if_smpc_params_lqr.w.dist = pd;
    if_smpc_params_lqr.w.w_max_norm = w_bar;
    
    %% Simulation
    if_smpc_lqr = indirectFeedback_SMPC_SF(if_smpc_params_lqr);
    disp(['Run M=' num2str(M) 'simulation']);
    m_STATES_X_if = [];
    m_NOM_STATES_S_if = [];
    m_INPUTS_U_if = [];
    m_FREE_INPUTS_C_if = [];
    m_VIOLATIONS_if = [];
    for j=1:M
        if mod(j,10) == 0; disp(['Sim. No ',num2str(j)]); end
        [x_traj_if,s_traj_if,u_traj_if,c_traj_if,~,~,avgcost_if(j),violation_traj_if] = trajectory_if_smpc(if_smpc_lqr,x_0,N_sim,W_all(((j-1)*nw+1):(j*nw),:));
        m_STATES_X_if = [m_STATES_X_if; x_traj_if];
        m_NOM_STATES_S_if = [m_NOM_STATES_S_if; s_traj_if];
        m_INPUTS_U_if = [m_INPUTS_U_if; u_traj_if];
        m_FREE_INPUTS_C_if = [m_FREE_INPUTS_C_if; c_traj_if];
        m_VIOLATIONS_if = [m_VIOLATIONS_if; violation_traj_if];
    end
    avg_violations_if = -1*ones(no_state_constr,size(m_VIOLATIONS_if,2));
    for idx_constr = 1:no_state_constr
        avg_violations_if(idx_constr,:) = sum(m_VIOLATIONS_if(idx_constr:no_state_constr:end,:))./M;
    end
    
    
else
    avgcost_if = nan;
    avg_violations_if = nan;
end


disp(' ');
disp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%');






%% Tightening for suboptimal controller, etc. 
if do_rs_tightening == true
    % Compute robust stochastic tightening
    disp('Robust stochastic tightening with suboptimal controller');
    F_tild_sub = F + G*K_sub;
    tic
    gamma_1_sub = exactTight(1,p,Ns,beta_conf,F_tild_sub,Acl_sub,D,pd,dist_lower_bound,dist_upper_bound,0);
    beta_all_sub = zeros(length(gamma_1_sub), max([Nrest,N-1]));
    for j = 1:max([Nrest,N-1])
        beta_all_sub(:,j) = robustStochTight(j,gamma_1_sub,F_tild_sub,Acl_sub,D,dist_lower_bound,dist_upper_bound);
    end
    if max(max(beta_all_sub)) > 1
        disp(['beta_all_sub exceeds 1: ', num2str(max(max(beta_all_sub)))]);
        warning("max(max(beta_all_sub)) > 1");
    end
    toc

    disp(['For K_sub: beta_max on x_1: ',num2str(max(beta_all_sub(1,:)))]);
    
end


%% Testsim: RS-MPC with suboptimal controller
if do_rs_sim == true
    disp(' ');
    disp('SIMULATION: RS-MPC with suboptimal K');
    
    %% Terminal Set
    disp('Compute Terminal Set');
    % Compute terminal set for RS-MPC
	[A_Zf_RS, b_Zf_RS, nu_RS] = computeTerminalSet(Acl_sub,D,N,F_tild_sub,gamma_1_sub,dist_lower_bound,dist_upper_bound,0);

    % Create parameter struct
    
    rs_mpc_params_sub.mpc.N = N;
    rs_mpc_params_sub.sys.A = A;
    rs_mpc_params_sub.sys.B = B;
    rs_mpc_params_sub.sys.D = D;
    rs_mpc_params_sub.sys.nx = nx;
    rs_mpc_params_sub.sys.nu = nu;
    rs_mpc_params_sub.sys.nw = nw;
    rs_mpc_params_sub.sys.K = K_sub;
    rs_mpc_params_sub.sys.Acl = Acl_sub;
    rs_mpc_params_sub.cost.Q = Q;
    rs_mpc_params_sub.cost.R = R;
    rs_mpc_params_sub.cost.P = P_sub;
    rs_mpc_params_sub.constraints.F = F;
    rs_mpc_params_sub.constraints.G = G;
    rs_mpc_params_sub.constraints.F_tilde = F_tild_sub;
    rs_mpc_params_sub.constraints.p = p;
    rs_mpc_params_sub.constraints.Hx = Hx;
    rs_mpc_params_sub.constraints.hx = hx;
    rs_mpc_params_sub.constraints.A_U0 = Hu;
    rs_mpc_params_sub.constraints.b_U0 = hu;
    rs_mpc_params_sub.constraints.det_input_constraint_bool = true;
    rs_mpc_params_sub.constraints.beta_i = beta_all_sub(:,1:N-1);
    rs_mpc_params_sub.constraints.Nbar = Nbar;
    rs_mpc_params_sub.constraints.A_Zf = A_Zf_RS;
    rs_mpc_params_sub.constraints.b_Zf = b_Zf_RS;
    rs_mpc_params_sub.w.dist_lower_bound = dist_lower_bound;
    rs_mpc_params_sub.w.dist_upper_bound = dist_upper_bound;
    rs_mpc_params_sub.w.dist = pd;
    rs_mpc_params_sub.w.w_max_norm = w_bar;
    
    %% Simulation
    rs_mpc_sub = robustStochastic_MPC(rs_mpc_params_sub);
    
    disp(['Run M=' num2str(M) 'simulation']);
    m_STATES_X_rs = [];
    m_INPUTS_U_rs = [];
    m_FREE_INPUTS_C_rs = [];
    m_VIOLATIONS_rs = [];
    for j=1:M
        if mod(j,10) == 0; disp(['Sim. No ',num2str(j)]); end
        [x_traj_rs,c_traj_rs,u_traj_rs,w_traj_rs,avgcost_rs(j),violation_traj_rs] = trajectory_rs_mpc(rs_mpc_sub,x_0,N_sim,W_all(((j-1)*nw+1):(j*nw),:));
        m_STATES_X_rs = [m_STATES_X_rs; x_traj_rs];
        m_INPUTS_U_rs = [m_INPUTS_U_rs; u_traj_rs];
        m_FREE_INPUTS_C_rs = [m_FREE_INPUTS_C_rs; c_traj_rs];
        m_VIOLATIONS_rs = [m_VIOLATIONS_rs; violation_traj_rs];
    end
    avg_violations_rs = -1*ones(no_state_constr,size(m_VIOLATIONS_rs,2));
    for idx_constr = 1:no_state_constr
        avg_violations_rs(idx_constr,:) = sum(m_VIOLATIONS_rs(idx_constr:no_state_constr:end,:))./M;
    end
    
else
    avgcost_rs = nan;
    avg_violations_rs = nan;
end


disp(' ');
disp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%');




%% new multistep tightening
if do_multistep_tightening == true
    disp('Tightening for Multistep SMPC with (suboptimal) K_ms');
    
    %% Compute tightening components
    max_beta_tilde_length = max([2*(N-1)-1,Nrest]); %(max length: 2Mmulti-1 with Mmulti=N-1)
    max_index_ms = max([N-1+Mmulti-1,2*Mmulti-1,Nrest]);
    F_tild_ms = F + G*K_ms;
    no_constr = size(F_tild_ms,1);
    tic
    % compute gamma_0 to gamma_M
    gamma_all_ms = exactTight_2(p,N-1,Ns,beta_conf,Acl_ms,F_tild_ms,D,pd,dist_lower_bound,dist_upper_bound,0);
    for i = 2:size(gamma_all_ms,2)
        for j = 1:size(gamma_all_ms,1)
            gamma_all_ms(j,i) = max(gamma_all_ms(j,i),gamma_all_ms(j,i-1));
        end
    end
    a_i_all_ms = [];
    for i = 0:max_beta_tilde_length
        a_i_temp =[];
        for j = 1:no_constr
            f_temp_j = (-F_tild_ms(j,:)*Acl_ms^(i)*D).';        % Need to max. real objective -> minus sign
           [~,fval,exitflag,output] = linprog(f_temp_j,Hw,hw,[],[],[],[],linprog_options);
           a_i_temp = [a_i_temp; -fval];                         % Want max. value -> need to take negative fval
           % DEBUG
            if exitflag == 1
                % Do nothing
            else
                disp(['Problem with optimization: exitflag = ',num2str(exitflag)]);
                disp(output.message);
            end
        end
        a_i_all_ms = [a_i_all_ms, a_i_temp];
    end
    toc        
    
    %% construct specific tightning sequence for selected Mmulti
    beta_tilde_ms = gamma_all_ms(:,1:Mmulti+1);
    for i = Mmulti+1:max_index_ms+1
        sum_a_ms = zeros(size(a_i_all_ms,1),1);
        for l = Mmulti:i-1
            sum_a_ms = sum_a_ms + a_i_all_ms(:,l+1);
        end
        beta_tilde_temp = gamma_all_ms(:,Mmulti+1) + sum_a_ms;
        beta_tilde_ms = [beta_tilde_ms, beta_tilde_temp];
    end
    
    disp(' ');
    disp(['For K_ms: beta_tilde_max on x_1: ',num2str(max(beta_tilde_ms(1,:)))]);
    
    
end
    




%% Testsim: simple Multistep SMPC with (suboptimal) tube controller K_ms
if do_simple_ms_sim
    disp(' ');
    disp('SIMULATION: simple multi-step SMPC WITH Kms (suboptimal)');
    
    %% Terminal Set
    disp('Compute Terminal Set');
    if max(max(beta_tilde_ms)) > 1
        error("max(beta_tilde_all_ms) > 1 --> Tightening resulting in empty set");
    end
    [A_Zf_MS, b_Zf_MS, nu_MS] = computeTerminalSet_MS_SMPC(Acl_ms,D,N,F_tild_ms,beta_tilde_ms(:,N),dist_lower_bound,dist_upper_bound,0);
    
    % Create Parameter Struct
    ms_smpc_params_ms.mpc.N = N;
    ms_smpc_params_ms.mpc.Mmulti = Mmulti;
    ms_smpc_params_ms.sys.A = A;
    ms_smpc_params_ms.sys.B = B;
    ms_smpc_params_ms.sys.D = D;
    ms_smpc_params_ms.sys.nx = nx;
    ms_smpc_params_ms.sys.nu = nu;
    ms_smpc_params_ms.sys.nw = nw;
    ms_smpc_params_ms.sys.K = K_ms;
    ms_smpc_params_ms.sys.Acl = Acl_ms;
    ms_smpc_params_ms.cost.Q = Q;
    ms_smpc_params_ms.cost.R = R;
    ms_smpc_params_ms.cost.P = P_ms;
    ms_smpc_params_ms.constraints.F = F;
    ms_smpc_params_ms.constraints.G = G;
    ms_smpc_params_ms.constraints.F_tilde = F_tild_ms;
    ms_smpc_params_ms.constraints.p = p;
    ms_smpc_params_ms.constraints.Hx = Hx;
    ms_smpc_params_ms.constraints.hx = hx;
    ms_smpc_params_ms.constraints.A_U0 = Hu;
    ms_smpc_params_ms.constraints.b_U0 = hu;
    ms_smpc_params_ms.constraints.beta_all = beta_tilde_ms;
    ms_smpc_params_ms.constraints.A_Zf = A_Zf_MS;
    ms_smpc_params_ms.constraints.b_Zf = b_Zf_MS;
    ms_smpc_params_ms.w.dist_lower_bound = dist_lower_bound;
    ms_smpc_params_ms.w.dist_upper_bound = dist_upper_bound;
    ms_smpc_params_ms.w.dist = pd;
    ms_smpc_params_ms.w.w_max_norm = w_bar;

    %% Simulation
    ms_smpc_ms = multistep_SMPC(ms_smpc_params_ms);
    %%
    disp(['Run M=' num2str(M) 'simulation']);
    m_STATES_X_ms = [];
    m_INPUTS_U_ms = [];
    m_FREE_INPUTS_C_ms = [];
    m_VIOLATIONS_ms = [];
    for j = 1:M
        if mod(j,10) == 0; disp(['Sim. No ',num2str(j)]); end
        [x_traj_ms, c_traj_ms, u_traj_ms,avgcost_ms(j),violation_traj_ms] = trajectory_ms_smpc(ms_smpc_ms,x_0,N_sim,W_all(((j-1)*nw+1):(j*nw),:));
        m_STATES_X_ms = [m_STATES_X_ms; x_traj_ms];
        m_INPUTS_U_ms = [m_INPUTS_U_ms; u_traj_ms];
        m_FREE_INPUTS_C_ms = [m_FREE_INPUTS_C_ms; c_traj_ms];
        m_VIOLATIONS_ms = [m_VIOLATIONS_ms; violation_traj_ms];
    end
    avg_violations_ms = -1*ones(no_state_constr,size(m_VIOLATIONS_ms,2));
    for idx_constr = 1:no_state_constr
        avg_violations_ms(idx_constr,:) = sum(m_VIOLATIONS_ms(idx_constr:no_state_constr:end,:))./M;
    end

else
    avgcost_ms = nan;
    avg_violations_ms = nan;
end


%%
if do_ms_sim == true
    disp(' ');
    disp('SIMULATION: (regular) MS-SMPC WITH Kms (suboptimal)');
    
    %% Terminal Set
    disp('Compute Terminal Set');
    if max(max(beta_tilde_ms)) > 1
        error("max(beta_tilde_all_ms) > 1 --> Tightening resulting in empty set");
    end
    
    % Time-varying terminal set:
    A_Zf_REMS = {};
    b_Zf_REMS = {};
    nu_REMS = {};
    for pred_step = 0:Mmulti-1
        disp(['Terminal Set for mod(k,Mmulti) = ',num2str(pred_step)]);
        [A_Zf_REMS_temp, b_Zf_REMS_temp, nu_REMS_temp] = computeTerminalSet_MS_SMPC(Acl_ms,D,N+pred_step,F_tild_ms,beta_tilde_ms(:,N+pred_step),dist_lower_bound,dist_upper_bound,0);
        A_Zf_REMS{pred_step+1} = A_Zf_REMS_temp;
        b_Zf_REMS{pred_step+1} = b_Zf_REMS_temp;
        nu_REMS{pred_step+1} = nu_REMS_temp;
    end
    
    % Create Parameter Struct
    rems_smpc_params.mpc.N = N;
    rems_smpc_params.mpc.Mmulti = Mmulti;
    rems_smpc_params.sys.A = A;
    rems_smpc_params.sys.B = B;
    rems_smpc_params.sys.D = D;
    rems_smpc_params.sys.nx = nx;
    rems_smpc_params.sys.nu = nu;
    rems_smpc_params.sys.nw = nw;
    rems_smpc_params.sys.K = K_ms;
    rems_smpc_params.sys.Acl = Acl_ms;
    rems_smpc_params.cost.Q = Q;
    rems_smpc_params.cost.R = R;
    rems_smpc_params.cost.P = P_ms;
    rems_smpc_params.constraints.F = F;
    rems_smpc_params.constraints.G = G;
    rems_smpc_params.constraints.F_tilde = F_tild_ms;
    rems_smpc_params.constraints.p = p;
    rems_smpc_params.constraints.Hx = Hx;
    rems_smpc_params.constraints.hx = hx;
    rems_smpc_params.constraints.A_U0 = Hu;
    rems_smpc_params.constraints.b_U0 = hu;
    rems_smpc_params.constraints.beta_all = beta_tilde_ms;
    rems_smpc_params.constraints.A_Zf = A_Zf_REMS;
    rems_smpc_params.constraints.b_Zf = b_Zf_REMS;
    rems_smpc_params.w.dist_lower_bound = dist_lower_bound;
    rems_smpc_params.w.dist_upper_bound = dist_upper_bound;
    rems_smpc_params.w.dist = pd;
    rems_smpc_params.w.w_max_norm = w_bar;
    
    %% Simulation   
    rems_smpc = multistep_reeval_SMPC(rems_smpc_params);
    %%
    disp(['Run M=' num2str(M) 'simulation']);
    m_STATES_X_rems = [];
    m_INPUTS_U_rems = [];
    m_NOM_STATES_S_rems = [];
    m_FREE_INPUTS_C_rems = [];
    m_VIOLATIONS_rems = [];
    for j = 1:M
        if mod(j,10) == 0; disp(['Sim. No ',num2str(j)]); end
        [x_traj_rems,c_traj_rems,u_traj_rems,s_nom_traj_rems,avgcost_rems(j),violation_traj_rems] = trajectory_rems_smpc(rems_smpc,x_0,N_sim,W_all(((j-1)*nw+1):(j*nw),:));
        m_STATES_X_rems = [m_STATES_X_rems; x_traj_rems];
        m_FREE_INPUTS_C_rems = [m_FREE_INPUTS_C_rems; c_traj_rems];
        m_INPUTS_U_rems = [m_INPUTS_U_rems; u_traj_rems];
        m_NOM_STATES_S_rems = [m_NOM_STATES_S_rems; s_nom_traj_rems];
        m_VIOLATIONS_rems = [m_VIOLATIONS_rems; violation_traj_rems];
        
    end
    avg_violations_rems = -1*ones(no_state_constr,size(m_VIOLATIONS_rems,2));
    for idx_constr = 1:no_state_constr
        avg_violations_rems(idx_constr,:) = sum(m_VIOLATIONS_rems(idx_constr:no_state_constr:end,:))./M;
    end
    
else
    avgcost_rems = nan;
    avg_violations_rems = nan;
end



disp(' ');
disp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%');







%% Testsim: linear tube controllers
if do_tube_sim == true
    disp(' ');
    disp('SIMULATION: Linear Tube Controllers');
    sys_params.A = A;
    sys_params.B = B;
    sys_params.D = D;
    sys_params.dist = pd;
    sys_params.nw = nw;
    sys_params.Hx = Hx;
    sys_params.hx = hx;
    sys_params.Hu = Hu;
    sys_params.hu = hu;
    sys_params.Q = Q;
    sys_params.R = R;
    
    disp(['Run M=' num2str(M) 'simulation']);
    m_VIOLATIONS_lqr = [];
    m_VIOLATIONS_Ksub = [];
    m_VIOLATIONS_Kms = [];
    m_STATES_X_lqr = [];
    m_STATES_X_ksub = [];
    m_STATES_X_kms = [];
    m_INPUTS_U_lqr = [];
    m_INPUTS_U_ksub = [];
    m_INPUTS_U_kms = [];
    for j = 1:M
        [x_traj_lqr, u_traj_lqr,avgcost_lqr_emp(j),violation_traj_lqr] = trajectory_linearGain(K_lqr,x_0,N_sim,sys_params,5,W_all(((j-1)*nw+1):(j*nw),:));
        [x_traj_ksub, u_traj_ksub,avgcost_sub_emp(j),violation_traj_Ksub] = trajectory_linearGain(K_sub,x_0,N_sim,sys_params,6,W_all(((j-1)*nw+1):(j*nw),:));
        [x_traj_kms, u_traj_kms,avgcost_Kms_emp(j),violation_traj_Kms] = trajectory_linearGain(K_ms,x_0,N_sim,sys_params,7,W_all(((j-1)*nw+1):(j*nw),:));
        m_STATES_X_lqr = [m_STATES_X_lqr;x_traj_lqr];
        m_STATES_X_ksub = [m_STATES_X_ksub;x_traj_ksub];
        m_STATES_X_kms = [m_STATES_X_kms;x_traj_kms];
        m_INPUTS_U_lqr = [m_INPUTS_U_lqr;u_traj_lqr];
        m_INPUTS_U_ksub = [m_INPUTS_U_ksub;u_traj_ksub];
        m_INPUTS_U_kms = [m_INPUTS_U_kms;u_traj_kms];
        m_VIOLATIONS_lqr = [m_VIOLATIONS_lqr; violation_traj_lqr];
        m_VIOLATIONS_Ksub = [m_VIOLATIONS_Ksub; violation_traj_Ksub];
        m_VIOLATIONS_Kms = [m_VIOLATIONS_Kms; violation_traj_Kms];
    end
    avg_violations_lqr = -1*ones(no_state_constr,size(m_VIOLATIONS_lqr,2));
    avg_violations_Ksub = -1*ones(no_state_constr,size(m_VIOLATIONS_Ksub,2));
    avg_violations_Kms = -1*ones(no_state_constr,size(m_VIOLATIONS_Kms,2));
    for idx_constr = 1:no_state_constr
        avg_violations_lqr(idx_constr,:) = sum(m_VIOLATIONS_lqr(idx_constr:no_state_constr:end,:))./M;
        avg_violations_Ksub(idx_constr,:) = sum(m_VIOLATIONS_Ksub(idx_constr:no_state_constr:end,:))./M;
        avg_violations_Kms(idx_constr,:) = sum(m_VIOLATIONS_Kms(idx_constr:no_state_constr:end,:))./M;
    end
    
end
    
    


%% Postprocessing for Performance Computation
% Settings:
avg_min_index = 51; % Only k = 50 and higher are considered in the average

% RS-MPC
if do_rs_sim == true
    summary_cost_vector_rs = [];
    for sim_real = 1:M
        summary_cost_ctr_rs = 0;
        summary_cost_length = length(avg_min_index:N_sim);
        for k_temp = avg_min_index:N_sim
            summary_x_rs_temp = m_STATES_X_rs((sim_real-1)*nx+1:sim_real*nx,k_temp);
            summary_u_rs_temp = m_INPUTS_U_rs((sim_real-1)*nu+1:sim_real*nu,k_temp);
            summary_cost_ctr_rs = summary_cost_ctr_rs + summary_x_rs_temp.'*Q*summary_x_rs_temp + summary_u_rs_temp.'*R*summary_u_rs_temp;
        end
        summary_cost_vector_rs(sim_real) = summary_cost_ctr_rs/summary_cost_length;
    end
    summary_avg_cost_rs = mean(summary_cost_vector_rs);
    summary_avg_violations_rs = mean(avg_violations_rs(avg_min_index:N_sim));
else
    summary_avg_cost_rs = nan;
    summary_avg_violations_rs = nan;
end

% IF-SMPC
if do_if_sim == true
    summary_cost_vector_if = [];
    for sim_real = 1:M
        summary_cost_ctr_if = 0;
        summary_cost_length = length(avg_min_index:N_sim);
        for k_temp = avg_min_index:N_sim
            summary_x_if_temp = m_STATES_X_if((sim_real-1)*nx+1:sim_real*nx,k_temp);
            summary_u_if_temp = m_INPUTS_U_if((sim_real-1)*nu+1:sim_real*nu,k_temp);
            summary_cost_ctr_if = summary_cost_ctr_if + summary_x_if_temp.'*Q*summary_x_if_temp + summary_u_if_temp.'*R*summary_u_if_temp;
        end
        summary_cost_vector_if(sim_real) = summary_cost_ctr_if/summary_cost_length;
    end
    summary_avg_cost_if = mean(summary_cost_vector_if);
    summary_avg_violations_if = mean(avg_violations_if(avg_min_index:N_sim));
else
    summary_avg_cost_if = nan;
    summary_avg_violations_if = nan;
end

% Simple Multistep SMPC
if do_simple_ms_sim == true
    summary_cost_vector_ms = [];
    for sim_real = 1:M
        summary_cost_ctr_ms = 0;
        summary_cost_length = length(avg_min_index:N_sim);
        for k_temp = avg_min_index:N_sim
            summary_x_ms_temp = m_STATES_X_ms((sim_real-1)*nx+1:sim_real*nx,k_temp);
            summary_u_ms_temp = m_INPUTS_U_ms((sim_real-1)*nu+1:sim_real*nu,k_temp);
            summary_cost_ctr_ms = summary_cost_ctr_ms + summary_x_ms_temp.'*Q*summary_x_ms_temp + summary_u_ms_temp.'*R*summary_u_ms_temp;
        end
        summary_cost_vector_ms(sim_real) = summary_cost_ctr_ms/summary_cost_length;
    end
    summary_avg_cost_ms = mean(summary_cost_vector_ms);
    summary_avg_violations_ms = mean(avg_violations_ms(avg_min_index:N_sim));
else
    summary_avg_cost_ms = nan;
    summary_avg_violations_ms = nan;
end

% (regular) MS-SMPC
if do_ms_sim == true
    summary_cost_vector_rems = [];
    for sim_real = 1:M
        summary_cost_ctr_rems = 0;
        summary_cost_length = length(avg_min_index:N_sim);
        for k_temp = avg_min_index:N_sim
            summary_x_rems_temp = m_STATES_X_rems((sim_real-1)*nx+1:sim_real*nx,k_temp);
            summary_u_rems_temp = m_INPUTS_U_rems((sim_real-1)*nu+1:sim_real*nu,k_temp);
            summary_cost_ctr_rems = summary_cost_ctr_rems + summary_x_rems_temp.'*Q*summary_x_rems_temp + summary_u_rems_temp.'*R*summary_u_rems_temp;
        end
        summary_cost_vector_rems(sim_real) = summary_cost_ctr_rems/summary_cost_length;
    end
    summary_avg_cost_rems = mean(summary_cost_vector_rems);
    summary_avg_violations_rems = mean(avg_violations_rems(avg_min_index:N_sim));
else
    summary_avg_cost_rems = nan;
    summary_avg_violations_rems = nan;
end

% Tube Controllers
if do_tube_sim == true
    summary_cost_vector_lqr = [];
    for sim_real = 1:M
        summary_cost_ctr_lqr = 0;
        summary_cost_length = length(avg_min_index:N_sim);
        for k_temp = avg_min_index:N_sim
            summary_x_lqr_temp = m_STATES_X_lqr((sim_real-1)*nx+1:sim_real*nx,k_temp);
            summary_u_lqr_temp = m_INPUTS_U_lqr((sim_real-1)*nu+1:sim_real*nu,k_temp);
            summary_cost_ctr_lqr = summary_cost_ctr_lqr + summary_x_lqr_temp.'*Q*summary_x_lqr_temp + summary_u_lqr_temp.'*R*summary_u_lqr_temp;
        end
        summary_cost_vector_lqr(sim_real) = summary_cost_ctr_lqr/summary_cost_length;
    end
    summary_avg_cost_lqr = mean(summary_cost_vector_lqr);
    summary_avg_violations_lqr = mean(avg_violations_lqr(avg_min_index:N_sim));

    summary_cost_vector_Ksub = [];
    for sim_real = 1:M
        summary_cost_ctr_Ksub = 0;
        summary_cost_length = length(avg_min_index:N_sim);
        for k_temp = avg_min_index:N_sim
            summary_x_Ksub_temp = m_STATES_X_ksub((sim_real-1)*nx+1:sim_real*nx,k_temp);
            summary_u_Ksub_temp = m_INPUTS_U_ksub((sim_real-1)*nu+1:sim_real*nu,k_temp);
            summary_cost_ctr_Ksub = summary_cost_ctr_Ksub + summary_x_Ksub_temp.'*Q*summary_x_Ksub_temp + summary_u_Ksub_temp.'*R*summary_u_Ksub_temp;
        end
        summary_cost_vector_Ksub(sim_real) = summary_cost_ctr_Ksub/summary_cost_length;
    end
    summary_avg_cost_Ksub = mean(summary_cost_vector_Ksub);
    summary_avg_violations_Ksub = mean(avg_violations_Ksub(avg_min_index:N_sim));

    summary_cost_vector_Kms = [];
    for sim_real = 1:M
        summary_cost_ctr_Kms = 0;
        summary_cost_length = length(avg_min_index:N_sim);
        for k_temp = avg_min_index:N_sim
            summary_x_Kms_temp = m_STATES_X_kms((sim_real-1)*nx+1:sim_real*nx,k_temp);
            summary_u_Kms_temp = m_INPUTS_U_kms((sim_real-1)*nu+1:sim_real*nu,k_temp);
            summary_cost_ctr_Kms = summary_cost_ctr_Kms + summary_x_Kms_temp.'*Q*summary_x_Kms_temp + summary_u_Kms_temp.'*R*summary_u_Kms_temp;
        end
        summary_cost_vector_Kms(sim_real) = summary_cost_ctr_Kms/summary_cost_length;
    end
    summary_avg_cost_Kms = mean(summary_cost_vector_Kms);
    summary_avg_violations_Kms = mean(avg_violations_Kms(avg_min_index:N_sim));
else
    summary_avg_cost_lqr = nan;
    summary_avg_violations_lqr = nan;
    summary_avg_cost_Ksub = nan;
    summary_avg_violations_Ksub = nan;
    summary_avg_cost_Kms = nan;
    summary_avg_violations_Kms = nan;
end



%% Display Performance Summary
disp(' ');
disp(' ');
disp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%');
disp('PERFORMANCE');
disp(['Mmulti = ',num2str(Mmulti)]);
disp([num2str(M), ' realisations']);
disp(['Avg. over k = ',num2str(avg_min_index-1),':',num2str(N_sim-1)]);

disp(' ');
disp(['Cost        IF with LQR: ',num2str(summary_avg_cost_if)]);
disp(['Cost        RS with Ksub: ',num2str(summary_avg_cost_rs)]);
disp(['Cost simple MS with Kms: ',num2str(summary_avg_cost_ms)]);
disp(['Cost        MS with Kms: ',num2str(summary_avg_cost_rems)]);

disp(' ');
disp('CONSTRAINT VIOLATIONS');
disp(['Violations        IF: ',num2str(summary_avg_violations_if*100),'%']);
disp(['Violations        RS: ',num2str(summary_avg_violations_rs*100),'%']);
disp(['Violations simple MS: ',num2str(summary_avg_violations_ms*100),'%']);
disp(['Violations        MS: ',num2str(summary_avg_violations_rems*100),'%']);

disp(' ');
disp('TUBE CONTROLLERS - Empirical Performance');
disp(['Cost (Emp.)  LQR: ',num2str(summary_avg_cost_lqr)]);
disp(['Cost (Emp.) Ksub: ',num2str(summary_avg_cost_Ksub)]);
disp(['Cost (Emp.)  Kms: ',num2str(summary_avg_cost_Kms)]);
disp(' ');
disp('TUBE CONTROLLERS - Constraint Violations');
disp(['Violations  LQR: ',num2str(summary_avg_violations_lqr*100),'%']);
disp(['Violations Ksub: ',num2str(summary_avg_violations_Ksub*100),'%']);
disp(['Violations  Kms: ',num2str(summary_avg_violations_Kms*100),'%']);




%% Plot Simulations
% Settings:
state_lim = [0,300,-0.7,0.8];       % axis limits for state trajectories
violations_lim = [0,300,-0.1,40];    % axis limits for constraint violations
font_size_text = 8;     % font size
state_width = 2*200;    % window width
state_height = 600;     % window height
x0 = 1;                 % window position
y0 = 1;                 % window position

% State Trajectories and Constraint Violations, 
figure();
set(gca, 'fontname','Arial','fontsize',font_size_text)
sgtitle('State Trajectories and Constraint Violations','interpreter','latex','fontname','Arial','fontsize',font_size_text+4);

if do_if_sim == true
    subplot(4,2,1);
    title('\bf{IF-SMPC}','interpreter','latex');
    hold on;
    for no_traj = 1:M
        plot(0:N_sim,m_STATES_X_if(nx*no_traj-nx+1,:),'color',[0 0.4470 0.7410],'linewidth',0.25);
    %     plot(0:N_sim,m_NOM_STATES_S_if(nx*no_traj-nx+1,:),'color',[0.5 0.5 0.5],'linewidth',0.25);
    end
    yline(hx(1)/Hx(1),'r','linewidth',1);
    grid on;
    xlabel('time step $k$','interpreter','latex');
    ylabel('$x_{(1)}$','interpreter','latex');
    axis(state_lim)
    set(gca, 'fontname','Arial','fontsize',font_size_text)
    
    subplot(4,2,2);
    title('\bf{IF-SMPC}','interpreter','latex');
    hold on;
    bar(0:N_sim,avg_violations_if*100,'EdgeColor',[0 0.4470 0.7410],'FaceColor',[0 0.4470 0.7410]);
    yline((1-p(1))*100,'r--','linewidth',1);
    grid on;
    xlabel('time step $k$','interpreter','latex');
    % ylabel('Violations [\%]','interpreter','latex');
    ylabel('Pr$[x_{(1)} > 0.1$] [\%]','interpreter','latex');
    axis(violations_lim)
    set(gca, 'fontname','Arial','fontsize',font_size_text)
end

if do_rs_sim == true
    subplot(4,2,3);
    title('\bf{RS-MPC}','interpreter','latex');
    hold on;
    for no_traj = 1:M
        plot(0:N_sim,m_STATES_X_rs(nx*no_traj-nx+1,:),'k','linewidth',0.25);
    end
    yline(hx(1)/Hx(1),'r','linewidth',1);
    grid on;
    xlabel('time step $k$','interpreter','latex');
    ylabel('$x_{(1)}$','interpreter','latex');
    axis(state_lim)
    set(gca, 'fontname','Arial','fontsize',font_size_text)
    
    subplot(4,2,4);
    title('\bf{RS-MPC}','interpreter','latex');
    hold on;
    bar(0:N_sim,avg_violations_rs*100,'EdgeColor','k','FaceColor','k');
    yline((1-p(1))*100,'r--','linewidth',1);
    grid on;
    xlabel('time step $k$','interpreter','latex');
    % ylabel('Violations [\%]','interpreter','latex');
    ylabel('Pr$[x_{(1)} > 0.1$] [\%]','interpreter','latex');
    axis(violations_lim)
    set(gca, 'fontname','Arial','fontsize',font_size_text)
end

if do_ms_sim == true
    subplot(4,2,5);
    title('\bf{(regular) MS-SMPC}','interpreter','latex');
    hold on;
    for no_traj = 1:M
        plot(0:N_sim,m_STATES_X_rems(nx*no_traj-nx+1,:),'color',[0.4660 0.6740 0.1880],'linewidth',0.25);
    end
    yline(hx(1)/Hx(1),'r','linewidth',1);
    grid on;
    xlabel('time step $k$','interpreter','latex');
    ylabel('$x_{(1)}$','interpreter','latex');
    axis(state_lim)
    set(gca, 'fontname','Arial','fontsize',font_size_text)
    
    subplot(4,2,6);
    title('\bf{(regular) MS-SMPC}','interpreter','latex');
    hold on;
    bar(0:N_sim,avg_violations_rems*100,'EdgeColor',[0.4660 0.6740 0.1880],'FaceColor',[0.4660 0.6740 0.1880]);
    yline((1-p(1))*100,'r--','linewidth',1);
    grid on;
    xlabel('time step $k$','interpreter','latex');
    ylabel('Pr$[x_{(1)} > 0.1$] [\%]','interpreter','latex');
    % ylabel('Violations [\%]','interpreter','latex');
    axis(violations_lim)
    set(gca, 'fontname','Arial','fontsize',font_size_text)
end
    
if do_simple_ms_sim == true
    subplot(4,2,7);
    title('\bf{Simple Multistep SMPC}','interpreter','latex');
    hold on;
    for no_traj = 1:M
        plot(0:N_sim,m_STATES_X_ms(nx*no_traj-nx+1,:),'color',[0.4940 0.1840 0.5560],'linewidth',0.25);
    end
    yline(hx(1)/Hx(1),'r','linewidth',1);
    grid on;
    xlabel('time step $k$','interpreter','latex');
    ylabel('$x_{(1)}$','interpreter','latex');
    axis(state_lim)
    set(gca, 'fontname','Arial','fontsize',font_size_text)

    subplot(4,2,8);
    title('\bf{Simple Multistep SMPC}','interpreter','latex');
    hold on;
    bar(0:N_sim,avg_violations_ms*100,'EdgeColor',[0.4940 0.1840 0.5560],'FaceColor',[0.4940 0.1840 0.5560]);
    yline((1-p(1))*100,'r--','linewidth',1);
    grid on;
    xlabel('time step $k$','interpreter','latex');
    ylabel('Pr$[x_{(1)} > 0.1$] [\%]','interpreter','latex');
    axis(violations_lim)
    set(gca, 'fontname','Arial','fontsize',font_size_text)
end

set(gcf,'position',[x0,y0,state_width,state_height])

% Input Trajectories 
figure()
set(gca, 'fontname','Arial','fontsize',font_size_text)
sgtitle('Input Trajectories','interpreter','latex','fontname','Arial','fontsize',font_size_text+4);

if do_if_sim == true
    subplot(4,1,1);
    title('\bf{IF-SMPC}','interpreter','latex');
    hold on;
    for no_traj = 1:M
        plot(0:N_sim-1,m_INPUTS_U_if(nu*no_traj-nu+1,:),'color',[0 0.4470 0.7410],'linewidth',0.25);
    end
    yline(hu(1)/Hu(1),'r','linewidth',1);
    yline(hu(2)/Hu(2),'r','linewidth',1);
    grid on;
    xlabel('time step $k$','interpreter','latex');
    ylabel('$u$','interpreter','latex');
    axis padded%([-30,330,-1.2,0.6])
    set(gca, 'fontname','Arial','fontsize',font_size_text)
end

if do_rs_sim == true
    subplot(4,1,2);
    title('\bf{RS-MPC}','interpreter','latex');
    hold on;
    for no_traj = 1:M
        plot(0:N_sim-1,m_INPUTS_U_rs(nu*no_traj-nu+1,:),'k','linewidth',0.25);end
    yline(hu(1)/Hu(1),'r','linewidth',1);
    yline(hu(2)/Hu(2),'r','linewidth',1);
    grid on;
    xlabel('time step $k$','interpreter','latex');
    ylabel('$u$','interpreter','latex');
    axis padded%([-30,330,-1.2,0.6])
    set(gca, 'fontname','Arial','fontsize',font_size_text)
end

if do_ms_sim == true
    subplot(4,1,4);
    title('\bf{(regular) MS-SMPC}','interpreter','latex');
    hold on;
    for no_traj = 1:M
        plot(0:N_sim-1,m_INPUTS_U_rems(nu*no_traj-nu+1,:),'color',[0.4660 0.6740 0.1880],'linewidth',0.25);
    end
    yline(hu(1)/Hu(1),'r','linewidth',1);
    yline(hu(2)/Hu(2),'r','linewidth',1);
    grid on;
    xlabel('time step $k$','interpreter','latex');
    ylabel('$u$','interpreter','latex');
    axis padded%([-30,330,-1.2,0.6])
    set(gca, 'fontname','Arial','fontsize',font_size_text)
end

if do_simple_ms_sim == true
    subplot(4,1,3);
    title('\bf{Simple Multistep SMPC}','interpreter','latex');
    hold on;
    for no_traj = 1:M
        plot(0:N_sim-1,m_INPUTS_U_ms(nu*no_traj-nu+1,:),'color',[0.4940 0.1840 0.5560],'linewidth',0.25);
    end
    yline(hu(1)/Hu(1),'r','linewidth',1);
    yline(hu(2)/Hu(2),'r','linewidth',1);
    grid on;
    xlabel('time step $k$','interpreter','latex');
    ylabel('$u$','interpreter','latex');
    axis padded%([-30,330,-1.2,0.6])
    set(gca, 'fontname','Arial','fontsize',font_size_text)
end

set(gcf,'position',[x0,y0,state_width,state_height])



%%
if do_logging == true
    save(workspace_path)
    disp("WORKSPACE SAVED.");
    disp("END LOGGING");
    diary off 
end

