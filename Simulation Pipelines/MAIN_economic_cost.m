%% SMPC pipeline - 4TH-ORDER INTEGRATOR WITH ECONOMIC COST
% Simulation Pipeline for Case 2

% add MOSEK to your MATLAB path

close all
clear
clc


% Simulation Settings follow below

%% Pipeline Settings
do_logging = 0;     % if true, terminal output and workspace will be logged/saved

do_rs = 1;          % do RS-MPC simulation
do_if = 1;          % do IF-SMPC simulation
do_ms = 1;          % do MS-SMPC simulation
do_simple_ms = 0;   % do simple multi-step SMPC simulation


%% Start Logging
if do_logging == true
    current_time = datetime('now');
    t_string = replace(string(current_time),':','-');
    % start logging the command line outputs
    workspace_path = strcat('Workspace ',t_string,'4th.mat');
    cmd_path = strcat('Command Window ',t_string,'4th.txt');
    diary(cmd_path)
    disp('START LOGGING');
end



disp(" ");
disp(" ");
disp("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
disp("%                                                                      %");
disp("%           SIMULATION: 4TH-ORDER INTEGRATOR (economic cost)           %");
disp("%                                                                      %");
disp("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
disp(" ");


%% Simulation Parameters
N = 30;             % MPC horizon length
N_sim = 300;        % Simulation length
Mmulti = 2;         % Multistep horizon length
x_0 = 0*[1;0;0;0];  % initial state
Nbar = 300;         %  = \bar{k} in paper: largest computed gamma_k
M = 1000;           % Number of simulation realisations
p = [0.9,1,1];      % probability level vector


%% Offline Parameter Computation
% Compute Offline parameters
tic
disp('Compute Offline Parameters');
params_sys_4th_order = systemParams_4th_order_economic(N,p);

params_rs_mpc = robustStochastic_MPC_offline(params_sys_4th_order,0);
params_if_smpc = indirectFeedback_SMPC_offline(params_sys_4th_order,Nbar,0);
params_ms_smpc = multistep_SMPC_offline(params_sys_4th_order,Mmulti,0);
params_rems_smpc = multistep_reeval_SMPC_offline(params_sys_4th_order,Mmulti,0);
toc


%% SMPC Setup
disp(' ');
disp('SMPC Setup');

rs_mpc_4th_order = robustStochastic_MPC_economic(params_rs_mpc,false);
if_smpc_sf_4th_order = indirectFeedback_SMPC_SF_economic(params_if_smpc,false);
ms_smpc_4th_order = multistep_SMPC_economic(params_ms_smpc,false);
rems_smpc_4th_order = multistep_reeval_SMPC_economic(params_rems_smpc,0);


%% Lookup table for variables
dist = params_sys_4th_order.w.dist;
nx = params_sys_4th_order.sys.nx;
nu = params_sys_4th_order.sys.nu;
nw = params_sys_4th_order.sys.nw;
A = params_sys_4th_order.sys.A;
B = params_sys_4th_order.sys.B;
D = params_sys_4th_order.sys.D;
K = params_sys_4th_order.sys.K;
Acl = A+B*K;
Hx = params_sys_4th_order.constraints.Hx;
hx = params_sys_4th_order.constraints.hx;
Hu = params_sys_4th_order.constraints.A_U0;
hu = params_sys_4th_order.constraints.b_U0;
F = params_sys_4th_order.constraints.F;
G = params_sys_4th_order.constraints.G;
F_tild = params_sys_4th_order.constraints.F_tilde;




%% Simulation (multiple realisations)

tic

disp(' ');
disp('SIMULATION (multiple realisations)');
no_state_constr = length(hx);

% RS-MPC
if do_rs == true
    STATES_X_rs_m = [];
    FREE_INPUTS_C_rs_m = [];
    INPUTS_U_rs_m = [];
    DISTURBANCES_W_rs_m = [];
    VIOLATIONS_rs_m = [];
end


% IF-SMPC
if do_if == true
    STATES_X_ifsf_m = [];
    NOM_STATES_S_ifsf_m = [];
    FREE_INPUTS_C_ifsf_m = [];
    NOM_INPUTS_Unom_ifsf_m = [];
    INPUTS_U_ifsf_m = [];
    DISTURBANCES_W_ifsf_m = [];
    VIOLATIONS_ifsf_m = [];
end


% simple Multistep SMPC
if do_simple_ms == true
    STATES_X_ms_m = [];
    FREE_INPUTS_C_ms_m = [];
    INPUTS_U_ms_m = [];
    DISTURBANCES_W_ms_m = [];
    VIOLATIONS_ms_m = [];
end


% (regular) Multistep SMPC
if do_ms == true
    STATES_X_rems_m = [];
    NOM_STATES_S_rems_m = [];
    NOM_STATES_X_rems_m = [];
    FREE_INPUTS_C_rems_m = [];
    INPUTS_U_rems_m = [];
    DISTURBANCES_W_rems_m = [];
    VIOLATIONS_rems_m = [];
end


for no_sim = 1:M
    if mod(no_sim,10)==0;fprintf('Simulation Number %d \n',no_sim);end
    % Create disturbance trajectory
    w_traj_m = random(dist,nw,N_sim);
    
    
    % RS-MPC
    if do_rs == true
        x_traj_rs_m = [];
        c_traj_rs_m = [];
        u_traj_rs_m = [];
        violations_traj_rs_m = [];

        x_temp_rs_m = x_0;
        x_traj_rs_m = x_0;
        violations_temp_rs_m = [];
        for idx_constr = 1:no_state_constr
            if Hx(idx_constr,:)*x_temp_rs_m <= hx(idx_constr)
                violations_temp_rs_m(idx_constr) = 0;
            else
                violations_temp_rs_m(idx_constr) = 1;
            end
        end
        violations_traj_rs_m = [violations_traj_rs_m, violations_temp_rs_m.'];
    end
    
    
    % IF-SMPC
    if do_if == true
        x_traj_ifsf_m = [];
        s_traj_ifsf_m = [];
        c_traj_ifsf_m = [];
        u_traj_ifsf_m = [];
        unom_traj_ifsf_m = [];
        violations_traj_ifsf_m = [];

        % Step k = 0
        k_ifsf_m = 0;
        x_temp_ifsf_m = x_0;
        s_temp_ifsf_m = x_0;
        x_traj_ifsf_m = x_temp_ifsf_m;
        s_traj_ifsf_m = s_temp_ifsf_m;
        violations_temp_ifsf_m = [];

        % Violation check
        for idx_constr = 1:no_state_constr
            if Hx(idx_constr,:)*x_temp_ifsf_m <= hx(idx_constr)
                violations_temp_ifsf_m(idx_constr) = 0;
            else
                violations_temp_ifsf_m(idx_constr) = 1;
            end
        end
        violations_traj_ifsf_m = [violations_traj_ifsf_m, violations_temp_ifsf_m.'];
    end
    
    
    % simple Multistep SMPC
    if do_simple_ms == true
        x_traj_ms_m = [];
        c_traj_ms_m = [];
        u_traj_ms_m = [];
        violations_traj_ms_m = [];
        
        % step k = 0
        tk_ms_m = 0;
        x_temp_ms_m = x_0;
        x_last_ms_m = x_0;
        x_traj_ms_m = [x_traj_ms_m, x_temp_ms_m];
        violations_temp_ms_m = [];
        for idx_constr = 1:no_state_constr
            if Hx(idx_constr,:)*x_temp_ms_m <= hx(idx_constr)
                violations_temp_ms_m(idx_constr) = 0;
            else
                violations_temp_ms_m(idx_constr) = 1;
            end
        end
        violations_traj_ms_m = [violations_traj_ms_m, violations_temp_ms_m.'];
    end
    
    
    % (regular) Multistep SMPC
    if do_ms == true
        x_traj_rems_m = [];
        s_nom_traj_rems_m = [];
        x_nom_traj_rems_m = [];
        c_traj_rems_m = [];
        u_traj_rems_m = [];
        violations_traj_rems_m = [];
        
        % step k = 0
        k_rems_m = 0;
        x_temp_rems_m = x_0;
        x_nom_temp_rems_m = x_0;
        x_traj_rems_m = [x_traj_rems_m, x_temp_rems_m];
        violations_temp_rems_m = [];
        for idx_constr = 1:no_state_constr
            if Hx(idx_constr,:)*x_temp_rems_m <= hx(idx_constr)
                violations_temp_rems_m(idx_constr) = 0;
            else
                violations_temp_rems_m(idx_constr) = 1;
            end
        end
        violations_traj_rems_m = [violations_traj_rems_m, violations_temp_rems_m.'];
    end
    

    for i = 1:N_sim
%         fprintf('Step %d: ',i);
        w_temp_m = w_traj_m(:,i);
        
        
        % RS-MPC
        if do_rs == true
            [c_opt_rs_m,s_i_opt_temp_rs_m,c_i_opt_temp_rs_m,error_code_rs_m] = rs_mpc_4th_order.solve(x_temp_rs_m);
            if error_code_rs_m==0
                %do nothing
            elseif error_code_rs_m == 3
                % iterations exceeded
                disp(['simulation no_sim = ',num2str(no_sim),' for step i = ',num2str(i)]);
                warning(yalmiperror(error_code_rs_m));
            else
                disp("Workbench: error detected with robustStochastic_MPC_economic");
                disp(['Error occurred in simulation no_sim = ',num2str(no_sim),' for step i = ',num2str(i)]);
                error(yalmiperror(error_code_rs_m));
            end

            u_temp_rs_m = K*x_temp_rs_m + c_opt_rs_m;
            c_traj_rs_m = [c_traj_rs_m, c_opt_rs_m];
            u_traj_rs_m = [u_traj_rs_m, u_temp_rs_m];


            x_temp_rs_m = Acl*x_temp_rs_m + B*c_opt_rs_m + D*w_temp_m;
            x_traj_rs_m=[x_traj_rs_m,x_temp_rs_m];
            violations_temp_rs_m = [];
            for idx_constr = 1:no_state_constr
                if Hx(idx_constr,:)*x_temp_rs_m <= hx(idx_constr)
                    violations_temp_rs_m(idx_constr) = 0;
                else
                    violations_temp_rs_m(idx_constr) = 1;
                end
            end
            violations_traj_rs_m = [violations_traj_rs_m, violations_temp_rs_m.'];
        end
        
        
        % IF-SMPC
        if do_if == true
            [u_0_opt_ifsf_m, c_opt_ifsf_m, s_opt_ifsf_m, ubar_opt_temp_ifsf_m, xbar_opt_temp_ifsf_m, error_code_ifsf_m] = if_smpc_sf_4th_order.solve(x_temp_ifsf_m, s_temp_ifsf_m, k_ifsf_m);
            if error_code_ifsf_m==0
                %do nothing
            elseif error_code_ifsf_m == 3
                % iterations exceeded
                disp(['simulation no_sim = ',num2str(no_sim),' for step i = ',num2str(i)]);
                warning(yalmiperror(error_code_ifsf_m));
            else
                disp("Workbench: error detected with indirectFeedback_SMPC_SF_economic");
                % Error Routine
                disp(['Error occurred in simulation no_sim = ',num2str(no_sim),' for step i = ',num2str(i)]);
                error(yalmiperror(error_code_ifsf_m));
            end

            % Update
            u_nom_temp_ifsf_m = K*s_temp_ifsf_m + c_opt_ifsf_m(:,1);
            x_temp_ifsf_m = A*x_temp_ifsf_m + B*u_0_opt_ifsf_m + D*w_temp_m;
            s_temp_ifsf_m = Acl*s_temp_ifsf_m + B*c_opt_ifsf_m(:,1);
            k_ifsf_m = k_ifsf_m + 1;

            % Violation Check
            violations_temp_ifsf_m = [];
            for idx_constr = 1:no_state_constr
                if Hx(idx_constr,:)*x_temp_ifsf_m <= hx(idx_constr)
                    violations_temp_ifsf_m(idx_constr) = 0;
                else
                    violations_temp_ifsf_m(idx_constr) = 1;
                end
            end
            violations_traj_ifsf_m = [violations_traj_ifsf_m, violations_temp_ifsf_m.'];

            % Store
            x_traj_ifsf_m = [x_traj_ifsf_m, x_temp_ifsf_m];
            s_traj_ifsf_m = [s_traj_ifsf_m, s_temp_ifsf_m];
            u_traj_ifsf_m = [u_traj_ifsf_m, u_0_opt_ifsf_m];
            c_traj_ifsf_m = [c_traj_ifsf_m, c_opt_ifsf_m(:,1)];
            unom_traj_ifsf_m = [unom_traj_ifsf_m, u_nom_temp_ifsf_m];
        end
        
        
        % simple Multistep SMPC
        if do_simple_ms == true
            pred_step_ms_m = mod(tk_ms_m,Mmulti);
            % Obtain input
            if pred_step_ms_m == 0
                % Re-solve the SMPC problem
                [c_k_opt_ms_m, s_opt_temp_ms_m, ubar_opt_temp_ms_m, xbar_opt_ms_m, error_code_ms_m] = ms_smpc_4th_order.solve(x_temp_ms_m, x_last_ms_m, tk_ms_m);
                if error_code_ms_m==0
                    %do nothing
                elseif error_code_ms_m == 3
                    % iterations exceeded
                    disp(['simulation no_sim = ',num2str(no_sim),' for step i = ',num2str(i)]);
                    warning(yalmiperror(error_code_ms_m));
                else
                    disp("Workbench: error detected with multistep_SMPC");
                    % Error Routine
                    disp(['Error occurred in simulation no_sim = ',num2str(no_sim),' for step i = ',num2str(i)]);
                    error(yalmiperror(error_code_ms_m));
                end
            % else: keep using previous sequence
            end
            c_temp_ms_m = c_k_opt_ms_m(:,pred_step_ms_m+1);
            u_temp_ms_m = K*x_temp_ms_m + c_temp_ms_m;
                        
            % Update System
            x_temp_ms_m = A*x_temp_ms_m + B*u_temp_ms_m + D*w_temp_m;
            x_last_ms_m = xbar_opt_ms_m(:,Mmulti+1);
            tk_ms_m = tk_ms_m + 1;
            
            % Violations Check
            violations_temp_ms_m = [];
            for idx_constr = 1:no_state_constr
                if Hx(idx_constr,:)*x_temp_ms_m <= hx(idx_constr)
                    violations_temp_ms_m(idx_constr) = 0;
                else
                    violations_temp_ms_m(idx_constr) = 1;
                end
            end
            violations_traj_ms_m = [violations_traj_ms_m, violations_temp_ms_m.'];
            
            % Store
            x_traj_ms_m = [x_traj_ms_m, x_temp_ms_m];
            c_traj_ms_m = [c_traj_ms_m, c_temp_ms_m];
            u_traj_ms_m = [u_traj_ms_m, u_temp_ms_m];
        end
        
        
        % (regular) Multistep SMPC
        if do_ms == true
            pred_step_rems_m = mod(k_rems_m,Mmulti);
            
            if k_rems_m < Mmulti
                M_k_rems_m = k_rems_m;
            else
                M_k_rems_m = Mmulti + pred_step_rems_m;
            end
            
            % prepare nominal states
            if pred_step_rems_m == 0
                s_nom_temp_rems_m = x_nom_temp_rems_m;
                x_nom_temp_rems_m = x_temp_rems_m;
            end
            x_nom_traj_rems_m = [x_nom_traj_rems_m, x_nom_temp_rems_m];
            s_nom_traj_rems_m = [s_nom_traj_rems_m, s_nom_temp_rems_m];
            
            % obtain input
            [c_opt_rems_m, s_opt_rems_m,ubar_opt_rems_m, xbar_opt_rems_m, x_cost_opt_rems_m, u_cost_opt_rems_m, error_code_rems_m] = rems_smpc_4th_order.solve(x_temp_rems_m, x_nom_temp_rems_m,s_nom_temp_rems_m,k_rems_m);
            if error_code_rems_m==0
                %do nothing
            elseif error_code_rems_m == 3
                % iterations exceeded
                disp(['simulation no_sim = ',num2str(no_sim),' for step i = ',num2str(i)]);
                disp(['pred_step_rems_m = ',num2str(pred_step_rems_m)]);
                disp(['k_rems_m = ',num2str(k_rems_m)]);
                warning(yalmiperror(error_code_rems_m));
            else
                disp("Workbench: error detected with reeval_multistep_SMPC");
                % Error Routine
                disp(['Error occurred in simulation no_sim = ',num2str(no_sim),' for step i = ',num2str(i)]);
                disp(['pred_step_rems_m = ',num2str(pred_step_rems_m)]);
                disp(['k_rems_m = ',num2str(k_rems_m)]);
                error(yalmiperror(error_code_rems_m));
            end
            c_temp_rems_m = c_opt_rems_m(:,1);
            u_temp_rems_m = K*x_temp_rems_m + c_temp_rems_m;
            
            % System Update
%             x_temp_rems_m = A*x_temp_rems_m + B*u_temp_rems_m + D*w_temp_m;
            x_temp_rems_m = Acl*x_temp_rems_m + B*c_opt_rems_m(:,1) + D*w_temp_m;
%             x_nom_temp_rems_m = Acl*x_nom_temp_rems_m + B*c_temp_rems_m;
%             s_nom_temp_rems_m = Acl*s_nom_temp_rems_m + B*c_temp_rems_m;
            x_nom_temp_rems_m = xbar_opt_rems_m(:,2);
            s_nom_temp_rems_m = s_opt_rems_m(:,2);
            
            k_rems_m = k_rems_m + 1;
            
            % Violations check
            violations_temp_rems_m = [];
            for idx_constr = 1:no_state_constr
                if Hx(idx_constr,:)*x_temp_rems_m <= hx(idx_constr)
                    violations_temp_rems_m(idx_constr) = 0;
                else
                    violations_temp_rems_m(idx_constr) = 1;
                end
            end
            violations_traj_rems_m = [violations_traj_rems_m, violations_temp_rems_m.'];
            
            x_traj_rems_m = [x_traj_rems_m, x_temp_rems_m];
            u_traj_rems_m = [u_traj_rems_m, u_temp_rems_m];
            c_traj_rems_m = [c_traj_rems_m, c_temp_rems_m];
            c_last_test = c_opt_rems_m;
        end
        
        
    end
    
    
    % RS-MPC
    if do_rs == true
        STATES_X_rs_m = [STATES_X_rs_m; x_traj_rs_m];
        FREE_INPUTS_C_rs_m = [FREE_INPUTS_C_rs_m; c_traj_rs_m];
        INPUTS_U_rs_m = [INPUTS_U_rs_m; u_traj_rs_m];
        DISTURBANCES_W_rs_m = [DISTURBANCES_W_rs_m; w_traj_m];
        VIOLATIONS_rs_m = [VIOLATIONS_rs_m; violations_traj_rs_m];
    end
    

    % IF-SMPC
    if do_if == true
        STATES_X_ifsf_m = [STATES_X_ifsf_m; x_traj_ifsf_m];
        NOM_STATES_S_ifsf_m = [NOM_STATES_S_ifsf_m; s_traj_ifsf_m];
        FREE_INPUTS_C_ifsf_m = [FREE_INPUTS_C_ifsf_m; c_traj_ifsf_m];
        NOM_INPUTS_Unom_ifsf_m = [NOM_INPUTS_Unom_ifsf_m; unom_traj_ifsf_m];
        INPUTS_U_ifsf_m = [INPUTS_U_ifsf_m; u_traj_ifsf_m];
        DISTURBANCES_W_ifsf_m = [DISTURBANCES_W_ifsf_m; w_traj_m];
        VIOLATIONS_ifsf_m = [VIOLATIONS_ifsf_m; violations_traj_ifsf_m];
    end
    
    
    % simple Multistep SMPC
    if do_simple_ms == true
        STATES_X_ms_m = [STATES_X_ms_m; x_traj_ms_m];
        FREE_INPUTS_C_ms_m = [FREE_INPUTS_C_ms_m; c_traj_ms_m];
        INPUTS_U_ms_m = [INPUTS_U_ms_m; u_traj_ms_m];
        DISTURBANCES_W_ms_m = [DISTURBANCES_W_ms_m; w_traj_m];
        VIOLATIONS_ms_m = [VIOLATIONS_ms_m; violations_traj_ms_m];
    end
    
    
    % (regular) Multistep SMPC
    if do_ms == true
        STATES_X_rems_m = [STATES_X_rems_m; x_traj_rems_m];
        NOM_STATES_S_rems_m = [NOM_STATES_S_rems_m; s_nom_traj_rems_m];
        NOM_STATES_X_rems_m = [NOM_STATES_X_rems_m; x_nom_traj_rems_m];
        FREE_INPUTS_C_rems_m = [FREE_INPUTS_C_rems_m; c_traj_rems_m];
        INPUTS_U_rems_m = [INPUTS_U_rems_m; u_traj_rems_m];
        DISTURBANCES_W_rems_m = [DISTURBANCES_W_rems_m; w_traj_m];
        VIOLATIONS_rems_m = [VIOLATIONS_rems_m; violations_traj_rems_m];
    end
    
end


%% Postprocessing for Performance Computation
% Settings:
avg_min_index = 51; % Only k = 50 and higher are considered in the average

% RS-MPC
if do_rs == true
    
    VIOLATIONS_FRAC_rs_m = -1*ones(no_state_constr,size(VIOLATIONS_rs_m,2));
    for idx_constr = 1:no_state_constr
        VIOLATIONS_FRAC_rs_m(idx_constr,:) = sum(VIOLATIONS_rs_m(idx_constr:no_state_constr:end,:))./M;
    end
    MEAN_x1_rs_m = mean(STATES_X_rs_m(1:nx:end,:),'all');
    summary_avg_violations_rs = mean(VIOLATIONS_FRAC_rs_m(avg_min_index:N_sim));
    summary_avg_cost_rs = 0.1 - mean(STATES_X_rs_m(1:nx:end,avg_min_index:N_sim),'all');
else
    summary_avg_violations_rs = nan;
    summary_avg_cost_rs = nan;
end


% IF-SMPC
if do_if == true
    VIOLATIONS_FRAC_ifsf_m = -1*ones(no_state_constr, size(VIOLATIONS_ifsf_m,2));
    for idx_constr = 1:no_state_constr
        VIOLATIONS_FRAC_ifsf_m(idx_constr,:) = sum(VIOLATIONS_ifsf_m(idx_constr:no_state_constr:end,:))./M;
    end
    MEAN_x1_ifsf_m = mean(STATES_X_ifsf_m(1:nx:end,:),'all');
    summary_avg_violations_if = mean(VIOLATIONS_FRAC_ifsf_m(avg_min_index:N_sim));
    summary_avg_cost_if = 0.1 - mean(STATES_X_ifsf_m(1:nx:end,avg_min_index:N_sim),'all');
else
    summary_avg_violations_if = nan;
    summary_avg_cost_if = nan;
end


% simple Multistep SMPC
if do_simple_ms == true
    VIOLATIONS_FRAC_ms_m = -1*ones(no_state_constr, size(VIOLATIONS_ms_m,2));
    for idx_constr = 1:no_state_constr
        VIOLATIONS_FRAC_ms_m(idx_constr,:) = sum(VIOLATIONS_ms_m(idx_constr:no_state_constr:end,:))./M;
    end
    MEAN_x1_ms_m = mean(STATES_X_ms_m(1:nx:end,:),'all');
    summary_avg_violations_ms = mean(VIOLATIONS_FRAC_ms_m(avg_min_index:N_sim));
    summary_avg_cost_ms = 0.1 - mean(STATES_X_ms_m(1:nx:end,avg_min_index:N_sim),'all');
else
    summary_avg_violations_ms = nan;
    summary_avg_cost_ms = nan;
end


% Re-evaluating/regular Multistep SMPC
if do_ms == true
    VIOLATIONS_FRAC_rems_m = -1*ones(no_state_constr, size(VIOLATIONS_rems_m,2));
    for idx_constr = 1:no_state_constr
        VIOLATIONS_FRAC_rems_m(idx_constr,:) = sum(VIOLATIONS_rems_m(idx_constr:no_state_constr:end,:))./M;
    end
    MEAN_x1_rems_m = mean(STATES_X_rems_m(1:nx:end,:),'all');
    summary_avg_violations_rems = mean(VIOLATIONS_FRAC_rems_m(avg_min_index:N_sim));
    summary_avg_cost_rems = 0.1 - mean(STATES_X_rems_m(1:nx:end,avg_min_index:N_sim),'all');
else
    summary_avg_violations_rems = nan;
    summary_avg_cost_rems = nan;
end


%% Display Performance Summary
disp(' ');
disp(' ');
disp("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
disp('PERFORMANCE SUMMARY');
disp(' ');
disp(['Mmulti = ',num2str(Mmulti)]);
disp([num2str(M), ' realisations']);
disp(['Avg. over k = ',num2str(avg_min_index-1),':',num2str(N_sim-1)]);
disp(' ');

disp(['Cost        RS: ',num2str(summary_avg_cost_rs)]);
disp(['Cost        IF: ',num2str(summary_avg_cost_if)]);
disp(['Cost simple MS: ',num2str(summary_avg_cost_ms)]);
disp(['Cost        MS: ',num2str(summary_avg_cost_rems)]);
disp(' ');
disp(['Cost factor IF/RS ',num2str(summary_avg_cost_if/summary_avg_cost_rs)]);
disp(['Cost factor simpleMS/RS ',num2str(summary_avg_cost_ms/summary_avg_cost_rs)]);
disp(['Cost factor MS/RS ',num2str(summary_avg_cost_rems/summary_avg_cost_rs)]);


disp(' ');
disp('CONSTRAINT VIOLATIONS');
disp(['Violations        RS: ',num2str(summary_avg_violations_rs*100), '%']);
disp(['Violations        IF: ',num2str(summary_avg_violations_if*100), '%']);
disp(['Violations simple MS: ',num2str(summary_avg_violations_ms*100), '%']);
disp(['Violations        MS: ',num2str(summary_avg_violations_rems*100), '%']);


%% Plot Simulations
% Settings:
lim_states = [0,300,-0.02,0.15];        % axis limits for state trajectories
lim_violations = [0,300,-0.1,20];  % axis limits for violation trajectories
font_size_text = 8;     % font size
state_width = 2*200;    % window width
state_height = 600;     % window height
x0 = 1;                 % window position
y0 = 1;                 % window position


% State Trajectories and Constraint Violations
figure();

set(gca, 'fontname','Arial','fontsize',font_size_text)
sgtitle('State Trajectories and Constraint Violations','interpreter','latex','fontname','Arial','fontsize',font_size_text+4);

if do_if == true
    subplot(4,2,1);
    title('\bf{IF-SMPC}','interpreter','latex');
    hold on;
    for no_traj = 1:M
        plot(0:N_sim,STATES_X_ifsf_m(nx*no_traj-nx+1,:),'color',[0 0.4470 0.7410],'linewidth',0.25);
    %     plot(0:N_sim,NOM_STATES_S_ifsf_m(nx*no_traj-nx+1,:),'color',[0.5 0.5 0.5],'linewidth',0.25);
    end
    yline(hx(1)/Hx(1),'r','linewidth',1);
    grid on;
    xlabel('time step $k$','interpreter','latex');
    ylabel('$x_{(1)}$','interpreter','latex');
    axis(lim_states)
    set(gca, 'fontname','Arial','fontsize',font_size_text)
    
    subplot(4,2,2)
    title('\bf{IF-SMPC}','interpreter','latex');
    hold on;
    bar(0:N_sim,VIOLATIONS_FRAC_ifsf_m*100,'EdgeColor',[0 0.4470 0.7410],'FaceColor',[0 0.4470 0.7410]);
    yline((1-p(1))*100,'r--','linewidth',1);
    grid on;
    xlabel('time step $k$','interpreter','latex');
    ylabel('Pr$[x_{(1)} > 0.1$] [\%]','interpreter','latex');
    axis(lim_violations)
    set(gca, 'fontname','Arial','fontsize',font_size_text)
end

if do_rs == true
    subplot(4,2,3);
    title('\bf{RS-MPC}','interpreter','latex');
    hold on;
    for no_traj = 1:M
        plot(0:N_sim,STATES_X_rs_m(nx*no_traj-nx+1,:),'k','linewidth',0.25);
    end
    yline(hx(1)/Hx(1),'r','linewidth',1);
    grid on;
    xlabel('time step $k$','interpreter','latex');
    ylabel('$x_{(1)}$','interpreter','latex');
    axis(lim_states)
    set(gca, 'fontname','Arial','fontsize',font_size_text)
    
    subplot(4,2,4);
    title('\bf{RS-MPC}','interpreter','latex');
    hold on;
    bar(0:N_sim,VIOLATIONS_FRAC_rs_m*100,'EdgeColor','k','FaceColor','k');
    yline((1-p(1))*100,'r--','linewidth',1);
    grid on;
    xlabel('time step $k$','interpreter','latex');
    ylabel('Pr$[x_{(1)} > 0.1$] [\%]','interpreter','latex');
    axis(lim_violations)
    set(gca, 'fontname','Arial','fontsize',font_size_text)
end

if do_ms == true
    subplot(4,2,5);
    title('\bf{(regular) MS-SMPC}','interpreter','latex');
    hold on;
    for no_traj = 1:M
        plot(0:N_sim,STATES_X_rems_m(nx*no_traj-nx+1,:),'color',[0.4660 0.6740 0.1880],'linewidth',0.25);
    end
    yline(hx(1)/Hx(1),'r','linewidth',1);
    grid on;
    xlabel('time step $k$','interpreter','latex');
    ylabel('$x_{(1)}$','interpreter','latex');
    axis(lim_states)
    set(gca, 'fontname','Arial','fontsize',font_size_text)
    
    subplot(4,2,6);
    title('\bf{(regular) MS-SMPC}','interpreter','latex');
    hold on;
    bar(0:N_sim,VIOLATIONS_FRAC_rems_m*100,'EdgeColor',[0.4660 0.6740 0.1880],'FaceColor',[0.4660 0.6740 0.1880]);
    yline((1-p(1))*100,'r--','linewidth',1);
    grid on;
    xlabel('time step $k$','interpreter','latex');
    ylabel('Pr$[x_{(1)} > 0.1$] [\%]','interpreter','latex');
    axis(lim_violations)
    set(gca, 'fontname','Arial','fontsize',font_size_text)
end

if do_simple_ms == true
    subplot(4,2,7);
    title('\bf{simple Multistep SMPC}','interpreter','latex');
    hold on;
    for no_traj = 1:M
        plot(0:N_sim,STATES_X_ms_m(nx*no_traj-nx+1,:),'color',[0.4940 0.1840 0.5560],'linewidth',0.25);
    end
    yline(hx(1)/Hx(1),'r','linewidth',1);
    grid on;
    xlabel('time step $k$','interpreter','latex');
    ylabel('$x_{(1)}$','interpreter','latex');
    axis(lim_states)
    set(gca, 'fontname','Arial','fontsize',font_size_text)
    
    subplot(4,2,8);
    title('\bf{simple Multistep SMPC}','interpreter','latex');
    hold on;
    bar(0:N_sim,VIOLATIONS_FRAC_ms_m*100,'EdgeColor',[0.4940 0.1840 0.5560],'FaceColor',[0.4940 0.1840 0.5560]);
    yline((1-p(1))*100,'r--','linewidth',1);
    grid on;
    xlabel('time step $k$','interpreter','latex');
    ylabel('Pr$[x_{(1)} > 0.1$] [\%]','interpreter','latex');
    axis(lim_violations)
    set(gca, 'fontname','Arial','fontsize',font_size_text)
end

set(gcf,'position',[x0,y0,state_width,state_height])



%% Logging
if do_logging == true
    save(workspace_path)
    disp(' ');
    disp(' ');
    disp("WORKSPACE SAVED.");
    disp("END LOGGING");
    diary off
end
