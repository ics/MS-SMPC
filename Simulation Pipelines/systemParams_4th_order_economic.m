function params = systemParams_4th_order_economic(N,p)


%% PARAMETERS
    % STOCHASTIC PARAMETERS
    Ns = 1e6;                        % no. of samples for scenario-based computation
    dist_lower_bound = -4;%-1;       % Paramters for uniform distribution
    dist_upper_bound = 4;%1;
    beta = 1e-4;                     % parameter for confidence = 1-beta for scenario-based computation
    pd = makedist('Uniform',dist_lower_bound, dist_upper_bound);

    % MPC PARAMETERS
%     N = 15;      % Horizon length - function input

    % SYSTEM
    Ts = 0.1;
    
    nx = 4;
    nu = 1;
    nw = 1;
    A = [1, Ts, Ts^2/2, Ts^3/6; ...
         0, 1, Ts, Ts^2/2; ...
         0, 0, 1, Ts; ...
         0, 0, 0, 1];
    B = [Ts^4/24; ...
         Ts^3/6; ...
         Ts^2/2; ...
         Ts];
    D = B; 
    
    % LINEAR CONTROLLER
    Q_lqr = 675*diag([1,0,0,0]);
    R_lqr = 0.1;
    [K,P_lqr,~] = dlqr(A,B,Q_lqr,R_lqr); 
    K = -K;     % dlqr determines K s.t. A-B*K stable. I use the notation A+B*K stable
    Acl = A+B*K;

    % ECONOMIC COST
    Q = [-1;0;0;0];
    R = 0;
    P = (Q.' / (eye(nx) - Acl)).';
    
    % STATE CONSTRAINTS
    Hx = 1/0.1*[1, 0, 0, 0]; %1/0.5*[1, 0, 0, 0]
    hx = 1;
    % INPUT CONSTRAINTS
    Hu = 1/20*[1; -1];
    hu = [1;1];
    det_input_constraint = true;
    % AUGMENTED CONSTRAINT MATRIX
    F = [Hx;...
         zeros(size(Hu,1),nx)];
    G = [zeros(size(Hx,1),nu);...
         Hu];
    F_tild = F + G*K;
    
    % MAX NORM OF Dw
    w_max = ones(nw,1)*max(abs([dist_lower_bound,dist_upper_bound]));
    w_bar = norm(D*w_max,2);
    
    
    %% GENERATE PARAMETER STRUCT
    % System Parameters
    params.sys.nx = nx;
    params.sys.nu = nu;
    params.sys.nw = nw;
    params.sys.A = A;
    params.sys.B = B;
    params.sys.D = D;
    params.sys.K = K;
    params.sys.Acl = Acl;
    
    % Cost
    params.cost.Q = Q;
    params.cost.R = R;
    params.cost.P = P;
    
    % Disturbance
    params.w.dist_lower_bound = dist_lower_bound;       % Disturbance support
    params.w.dist_upper_bound = dist_upper_bound;
    params.w.dist = pd;         % distribution object for w (i.i.d)
    params.w.w_max_norm = w_bar;
    
    % Constraints
    params.constraints.F = F;
    params.constraints.G = G;
    params.constraints.F_tilde = F_tild;
    params.constraints.p = p;             % constraint fulfillment probability
    params.constraints.Hx = Hx;
    params.constraints.hx = hx;
    params.constraints.A_U0 = Hu;
    params.constraints.b_U0 = hu;
    params.constraints.det_input_constraint_bool = det_input_constraint;
    
     % MPC
     params.mpc.N = N;
    
    % Stocastic Parameters
    params.stoch.Ns = Ns;           % number of samples for scenario approach
    params.stoch.conf = beta;       % confidence for scenario approach
    
end

